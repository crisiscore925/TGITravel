package com.traver.travel.myanmar.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.traver.travel.myanmar.BuildConfig;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.activities.MainActivity;
import com.traver.travel.myanmar.activities.SignUpActivity;
import com.traver.travel.myanmar.activities.UploadActivity;
import com.traver.travel.myanmar.data.vos.UserVO;
import com.traver.travel.myanmar.utils.ConnectivityReceiver;
import com.traver.travel.myanmar.utils.PrefUtils;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {

    @BindView(R.id.upload_btn)
    Button uploadButton;
    @BindView(R.id.sign_in_btn)
    Button signInButton;
    @BindView(R.id.logout_btn)
    Button logoutButton;
    @BindView(R.id.sign_out_layout)
    RelativeLayout signOutLayout;
    @BindView(R.id.sign_in_layout)
    RelativeLayout signInLayout;
    @BindView(R.id.profile_image)
    ImageView profileImage;
    @BindView(R.id.profile_name)
    TextView profileName;
    @BindView(R.id.version_name)
    TextView versionName;
    private CollectionReference collectionReference;
    private Dialog dialog;

    public ProfileFragment() {

    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ButterKnife.bind(this, view);
        FirebaseFirestore mFireStore = FirebaseFirestore.getInstance();
        collectionReference = mFireStore.collection(UserVO.UPLOAD_USER);
        FirebaseFirestore.setLoggingEnabled(true);

        versionName.setText(getResources().getString(R.string.version).concat(" ").concat(BuildConfig.VERSION_NAME));

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSignIn();
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UploadActivity.class);
                startActivity(intent);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityReceiver.isConnected()) {
                    updateUser(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
                    dialog = new Dialog(Objects.requireNonNull(getContext()));
                    dialog.setContentView(R.layout.loading_dialog);
                    dialog.setCancelable(true);
                    if (dialog.getWindow() != null) {
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    }
                    dialog.show();
                } else {
                    final Snackbar snackbar = Snackbar.make(Objects.requireNonNull(getView()).findViewById(R.id.placeSnackBar), "No Internet Connection", Snackbar.LENGTH_INDEFINITE);
                    snackbar.getView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(getResources().getColor(R.color.colorWhite));
                    snackbar.show();
                }

            }
        });

        showView();
        return view;

    }

    private void showView() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            signInLayout.setVisibility(View.GONE);
            signOutLayout.setVisibility(View.VISIBLE);
            showProfile();
        } else {
            signOutLayout.setVisibility(View.GONE);
            signInLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            showProfile();
        }
    }

    private void uploadUser(FirebaseUser firebaseUser) {
        final UserVO user = new UserVO();
        user.setUserId(firebaseUser.getUid());
        ArrayList<String> favList = new ArrayList<>();
        user.setUserFavList(favList);
        collectionReference.document(firebaseUser.getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                PrefUtils.setUser(getContext(), user);
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (aVoid != null) {
                    PrefUtils.setUser(getContext(), user);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                PrefUtils.setUser(getContext(), user);
            }
        });
    }

    private void updateUser(FirebaseUser firebaseUser) {
        UserVO user = new UserVO();
        user.setUserId(firebaseUser.getUid());
        ArrayList<String> favList;
        favList = PrefUtils.getUserBookmarks(getContext());
        user.setUserFavList(favList);
        collectionReference.document(firebaseUser.getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                FirebaseAuth.getInstance().signOut();
                dialog.hide();
                PrefUtils.removeUserData(getContext());
                showView();
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (aVoid != null) {
                    FirebaseAuth.getInstance().signOut();
                    dialog.hide();
                    PrefUtils.removeUserData(getContext());
                    showView();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                FirebaseAuth.getInstance().signOut();
                dialog.hide();
                PrefUtils.removeUserData(getContext());
                showView();
            }
        });
    }


    private void showProfile() {
        String facebookUserId = "";
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            if (firebaseUser.getDisplayName() != null && !firebaseUser.getDisplayName().equals("")) {
                profileName.setText(firebaseUser.getDisplayName());
            } else {
                profileName.setText(firebaseUser.getEmail());
            }

            for (UserInfo profile : firebaseUser.getProviderData()) {
                // check if the provider id matches "facebook.com"
                if (FacebookAuthProvider.PROVIDER_ID.equals(profile.getProviderId())) {
                    facebookUserId = profile.getUid();
                }
            }
            if (!facebookUserId.equals("")) {
                String photoUrl = "https://graph.facebook.com/" + facebookUserId + "/picture?height=500";
                Glide.with(Objects.requireNonNull(getContext())).load(photoUrl).apply(RequestOptions.bitmapTransform(new CenterCrop())).into(profileImage);
            } else if (firebaseUser.getPhotoUrl() != null) {
                Glide.with(Objects.requireNonNull(getContext()))
                        .load(firebaseUser.getPhotoUrl().toString())
                        .apply(RequestOptions.bitmapTransform(new CenterCrop()))
                        .into(profileImage);
            } else {
                Glide.with(Objects.requireNonNull(getContext()))
                        .load(R.drawable.profile_dummy)
                        .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                        .into(profileImage);
            }
            uploadButton.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MainActivity.RC_SIGN_IN) {
            if (resultCode == RESULT_OK && FirebaseAuth.getInstance().getCurrentUser() != null) {
                showView();
                DocumentReference documentReference = collectionReference.document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
                documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot snapshot) {
                        if (snapshot.exists()) {
                            PrefUtils.setUser(getContext(), snapshot.toObject(UserVO.class));
                        } else {
                            uploadUser(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
                        }
                    }
                });
            }
        }
    }

    private void startSignIn() {
        Intent intent = new Intent(getActivity(), SignUpActivity.class);
        startActivityForResult(intent, MainActivity.RC_SIGN_IN);
    }
}
