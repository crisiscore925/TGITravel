package com.traver.travel.myanmar.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.appyvet.materialrangebar.RangeBar;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.activities.AccommodationListActivity;
import com.traver.travel.myanmar.data.vos.AccommodationVO;
import com.traver.travel.myanmar.data.vos.FiltersVO;
import com.traver.travel.myanmar.utils.ConnectivityReceiver;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.honorato.multistatetogglebutton.ToggleButton;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FilterFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener{

    @BindView(R.id.search_accommodation_spinner)
    Spinner accommodationSpinner;
    @BindView(R.id.search_city_spinner)
    Spinner citySpinner;
    @BindView(R.id.search_place_spinner)
    Spinner placeSpinner;
    @BindView(R.id.mstb_multi_currency)
    MultiStateToggleButton mstbCurrency;
    @BindView(R.id.search_price)
    TextView searchPrice;
    @BindView(R.id.search_range_bar)
    RangeBar priceRangeBar;
    @BindView(R.id.search_check_box)
    CheckBox partTimeCheckBox;
    @BindView(R.id.search_btn)
    Button searchBtn;
    private ArrayAdapter<CharSequence> placeAdapter;
    private double minPriceValue;
    private double maxPriceValue;
    private String currenySign;

    private boolean checkState = false;

    public FilterFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        ButterKnife.bind(this, view);

        mstbCurrency.setElements(R.array.currency_array, 0);
        mstbCurrency.enableMultipleChoice(false);
        mstbCurrency.setOnValueChangedListener(new ToggleButton.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (value == 0) {
                    currenySign = getResources().getString(R.string.dollar_sign);
                } else {
                    currenySign = getResources().getString(R.string.kyats_sign);
                }
                setPriceView();
            }
        });

        setPriceView();

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int arrayPlace = getPlaceArray(i);
                placeAdapter = ArrayAdapter.createFromResource(Objects.requireNonNull(getContext()), arrayPlace, android.R.layout.simple_spinner_item);
                placeSpinner.setAdapter(placeAdapter);
                placeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        priceRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                setPriceView();
            }
        });

        partTimeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                checkState = compoundButton.isChecked();
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AccommodationListActivity.class);
                intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.FILTERQUERYID);
                intent.putExtra(AccommodationVO.ACCOMMODATION_CATEGORY , getFilters().getAccommodationCategory());
                intent.putExtra(AccommodationVO.ACCOMMODATION_CITY , getFilters().getAccommodationCity());
                intent.putExtra(AccommodationVO.ACCOMMODATION_PLACE , getFilters().getAccommodationPlace());
                intent.putExtra(AccommodationVO.ACCOMMODATION_MIN_PRICE , getFilters().getMinPrice());
                intent.putExtra(AccommodationVO.ACCOMMODATION_MAX_PRICE , getFilters().getMaxPrice());
                intent.putExtra(AccommodationVO.ACCOMMODATION_PART_TIME , getFilters().getPartTime());
                startActivity(intent);
            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setPriceView() {
        String minPrice = priceRangeBar.getLeftPinValue();
        String maxPrice = priceRangeBar.getRightPinValue();
        if (currenySign == null || currenySign.equals("")) {
            currenySign = getResources().getString(R.string.dollar_sign);
        }
        String priceRange;
        if (currenySign.equals(getResources().getString(R.string.dollar_sign))) {
            priceRange = minPrice.concat(" ").concat("-").concat(" ").concat(maxPrice).concat(" ").concat(currenySign);
            minPriceValue = Integer.parseInt(minPrice) * AccommodationVO.CURRENCY_EXCHANGE;
            maxPriceValue = Integer.parseInt(maxPrice) * AccommodationVO.CURRENCY_EXCHANGE;
        } else {
            if (Integer.parseInt(minPrice) == 0) {
                priceRange = minPrice.concat(" ").concat("-").concat(" ").concat(maxPrice).concat(",000").concat(" ").concat(currenySign);
            } else {
                priceRange = minPrice.concat(",000").concat(" ").concat("-").concat(" ").concat(maxPrice).concat(",000").concat(" ").concat(currenySign);
            }
            minPriceValue = Integer.parseInt(minPrice) * 1000;
            maxPriceValue = Integer.parseInt(maxPrice) * 1000;
        }
        searchPrice.setText(priceRange);
    }

    private int getPlaceArray(int position) {

        switch (position) {
            case 0:
                return R.array.anywhere_array;

            case 1:
                return R.array.yangon_array;

            case 2:
                return R.array.mandalay_array;

            case 3:
                return R.array.bagan_array;

            case 4:
                return R.array.kyaik_hti_yoe_array;

            case 5:
                return R.array.hpa_an_array;

            case 6:
                return R.array.bago_array;


            case 7:
                return R.array.kalaw_array;

            case 8:
                return R.array.hsipaw_array;

            case 9:
                return R.array.taunggyi_array;


            case 10:
                return R.array.inle_array;


            case 11:
                return R.array.pyinoolwin_array;


            case 12:
                return R.array.pyay_array;


            case 13:
                return R.array.mt_naw_bu_baw_array;


            case 14:
                return R.array.gaw_yan_gyi_array;


            case 15:
                return R.array.chaung_tha_array;

            case 16:
                return R.array.ngwe_saung_array;


            case 17:
                return R.array.ngapali_array;


            case 18:
                return R.array.kaw_thoung_array;

            case 19:
                return R.array.myaik_array;

            default:
                return R.array.anywhere_array;

        }

    }

    public FiltersVO getFilters() {
        FiltersVO filters = new FiltersVO();
        filters.setAccommodationCategory(getSelectedType());
        filters.setAccommodationCity(getSelectedCity());
        filters.setAccommodationPlace(getSelectedPlace());
        filters.setMinPrice(minPriceValue);
        filters.setMaxPrice(maxPriceValue);
        filters.setPartTime(getPartTimeState());
        return filters;
    }

    public String getSelectedType() {
        String selected = (String) accommodationSpinner.getSelectedItem();
        if (selected.equals(getResources().getString(R.string.any))) {
            return null;
        } else {
            return selected;
        }
    }

    public String getSelectedCity() {
        String selected = (String) citySpinner.getSelectedItem();
        if (selected.equals(getResources().getString(R.string.anywhere))) {
            return null;
        } else {
            return selected;
        }
    }

    public String getSelectedPlace() {
        String selected = (String) placeSpinner.getSelectedItem();
        if (selected.equals(getResources().getString(R.string.anywhere))) {
            return null;
        } else {
            return selected;
        }
    }

    public boolean getPartTimeState() {
        return checkState;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
