package com.traver.travel.myanmar.data.vos;

public class LocationVO {
    public double lat;
    public double lang;

    public LocationVO() {
    }

    public LocationVO(double lat, double lang) {
        this.lat = lat;
        this.lang = lang;
    }

    @Override
    public String toString() {
        return lat + "," + lang;
    }
}
