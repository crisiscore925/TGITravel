package com.traver.travel.myanmar.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.traver.travel.myanmar.data.vos.RatingVO;
import com.traver.travel.myanmar.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class RatingDialogFragment extends DialogFragment {

    public static final String TAG = "RatingDialog";
    @BindView(R.id.rating_dialog_bar)
    MaterialRatingBar mRatingBar;
    @BindView(R.id.rating_dialog_edittext)
    EditText mRatingText;
    private RatingListener mRatingListener;

    public interface RatingListener {
        void onRating(RatingVO rating);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rating_dialog_layout, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof RatingListener) {
            mRatingListener = (RatingListener) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//            getDialog().getWindow().setLayout(
//                    ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @OnClick(R.id.rating_dialog_submit)
    public void onSubmitClicked(View view) {
        RatingVO rating = new RatingVO(
                Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()),
                mRatingBar.getRating(),
                mRatingText.getText().toString());

        if (mRatingListener != null && (mRatingBar.getRating() > 0) || !mRatingText.getText().toString().equals("")) {
            mRatingListener.onRating(rating);
            dismiss();
        } else {
           Toast toast = Toast.makeText(getContext(), "Please Select a RatingVO Point or Leave a Comment", Toast.LENGTH_SHORT);
           toast.getView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
           toast.getView().setPadding(48 , 24 ,48 , 32);
           toast.show();
        }
    }

    @OnClick(R.id.rating_dialog_cancel)
    public void onCancelClicked(View view) {
        dismiss();
    }
}
