package com.traver.travel.myanmar.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.Query;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.activities.AccommodationInfoActivity;
import com.traver.travel.myanmar.activities.UploadActivity;
import com.traver.travel.myanmar.adapters.AccommodationListAdapter;
import com.traver.travel.myanmar.data.vos.UserVO;
import com.traver.travel.myanmar.utils.ConnectivityReceiver;
import com.traver.travel.myanmar.utils.PrefUtils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SavedFragment extends Fragment implements AccommodationListAdapter.OnAccommodationSelectedListener , ConnectivityReceiver.ConnectivityReceiverListener {

    @BindView(R.id.no_favourite_layout)
    LinearLayout noFavouriteLayout;
    @BindView(R.id.start_explore_btn)
    Button startExploreBtn;
    @BindView(R.id.favourite_label)
    TextView favouriteLabel;
    @BindView(R.id.favourite_rv)
    RecyclerView favouriteRV;
    @BindView(R.id.favourite_loading)
    AVLoadingIndicatorView avLoadingIndicatorView;
    private StartExploreClickListener listener;
    private AccommodationListAdapter adapter;
    private ArrayList<DocumentSnapshot> documentSnapshots;

    public SavedFragment() {

    }


    public interface StartExploreClickListener {
        void onExploreClicked();
    }

    public static SavedFragment newInstance() {
        return new SavedFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (StartExploreClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_saved, container, false);
        ButterKnife.bind(this, view);


        ArrayList<String> favList;
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            favList = PrefUtils.getUserBookmarks(getContext());
        } else {
            favList = new ArrayList<>();
        }
        checkFavList(favList);
        startExploreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onExploreClicked();
            }
        });
        return view;
    }

    void checkFavList(ArrayList<String> favList) {
        if (favList.size() > 0) {
            starAnim();
            noFavouriteLayout.setVisibility(View.GONE);
            favouriteLabel.setVisibility(View.VISIBLE);
            favouriteRV.setVisibility(View.VISIBLE);
            documentSnapshots = new ArrayList<>();
            favouriteRV.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new AccommodationListAdapter(getContext(), this, documentSnapshots , null);
            favouriteRV.setAdapter(adapter);
            getSnapshots(favList);
        } else {
            stopAnim();
            noFavouriteLayout.setVisibility(View.VISIBLE);
            favouriteLabel.setVisibility(View.GONE);
            favouriteRV.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
    }

    private void getSnapshots(ArrayList<String> favList) {
        CollectionReference collectionReference = FirebaseFirestore.getInstance().collection(UploadActivity.UPLOADREF);
        for (int i = 0; i < favList.size(); i++) {
            DocumentReference documentReference = collectionReference.document(favList.get(i));
            documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot snapshot) {
                    documentSnapshots.add(snapshot);
                    adapter.notifyDataSetChanged();
                    stopAnim();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    void starAnim() {
        avLoadingIndicatorView.show();
    }

    void stopAnim() {
        avLoadingIndicatorView.hide();
    }

    @Override
    public void onAccommodationSelected(DocumentSnapshot snapshot) {
        Intent intent = new Intent(getContext(), AccommodationInfoActivity.class);
        intent.putExtra(AccommodationInfoActivity.KEY_ACCOMMODATION_ID, snapshot.getId());
        startActivity(intent);
    }

    @Override
    public void onAccommodationFavourited(String snapshotID) {

    }

    @Override
    public void onAccommodationRemoved(String snapshotID, int position) {
        PrefUtils.removeUserBookmarks(getContext(), snapshotID);
        documentSnapshots.remove(position);
        updateUser(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
        adapter.notifyDataSetChanged();
    }

    private void updateUser(FirebaseUser firebaseUser) {
        UserVO user = new UserVO();
        user.setUserId(firebaseUser.getUid());
        ArrayList<String> favList;
        favList = PrefUtils.getUserBookmarks(getContext());
        user.setUserFavList(favList);
        CollectionReference collectionReference = FirebaseFirestore.getInstance().collection(UserVO.UPLOAD_USER);
        collectionReference.document(firebaseUser.getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (aVoid != null) {
                    Toast.makeText(getContext(), "Successfully remove from your favourites", Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
