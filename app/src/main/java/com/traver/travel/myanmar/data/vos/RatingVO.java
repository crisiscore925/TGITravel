package com.traver.travel.myanmar.data.vos;

import android.text.TextUtils;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.ServerTimestamp;
import java.util.Date;

public class RatingVO {

    private String userId;
    private String userName;
    private String userPhoto;
    private double rating;
    private String text;
    private @ServerTimestamp Date timestamp;

    public RatingVO() {
    }

    public RatingVO(FirebaseUser user , double rating, String text ) {
        this.userId = user.getUid();
        if (user.getPhotoUrl() != null){
            this.userPhoto = user.getPhotoUrl().toString();
        }
        this.userName = user.getDisplayName();
        if (TextUtils.isEmpty(this.userName)) {
            this.userName = user.getEmail();
        }
        this.rating = rating;
        this.text = text;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }
}
