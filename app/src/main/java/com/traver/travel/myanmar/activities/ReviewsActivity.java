package com.traver.travel.myanmar.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.Transaction;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.adapters.RatingAdapter;
import com.traver.travel.myanmar.data.vos.AccommodationVO;
import com.traver.travel.myanmar.data.vos.RatingVO;
import com.traver.travel.myanmar.utils.RatingDialogFragment;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ReviewsActivity extends AppCompatActivity implements RatingDialogFragment.RatingListener {

    @BindView(R.id.review_total_count)
    TextView reviewTotalCount;
    @BindView(R.id.review_total_rating)
    MaterialRatingBar reviewRatingBar;
    @BindView(R.id.review_list_RV)
    RecyclerView reviewListRV;
    @BindView(R.id.review_empty)
    ImageView mEmptyView;


    private DocumentReference mAccommodationRef;
    private FirebaseFirestore mFireStore;
    private RatingAdapter mRatingAdapter;
    private RatingDialogFragment mRatingDialog;
    private int newNumberRatings;
    private double newAvgRating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }else {
            setTheme(R.style.AppThemeDark);
        }
        setContentView(R.layout.activity_reviews);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
        }

        mFireStore = FirebaseFirestore.getInstance();
        String accommodationID = getIntent().getStringExtra(AccommodationInfoActivity.KEY_ACCOMMODATION_ID);
        int accommodationReviewNumber = getIntent().getIntExtra(AccommodationInfoActivity.KEY_ACCOMMODATION_REVIEW_NUMBER , -1);
        double accommodationReviewAvg = getIntent().getDoubleExtra(AccommodationInfoActivity.KEY_ACCOMMODATION_REVIEW_AVG , 0.0);
        mAccommodationRef = mFireStore.collection(UploadActivity.UPLOADREF).document(accommodationID);
        Query ratingsQuery = mAccommodationRef
                .collection("ratings")
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .limit(50);
        mRatingAdapter = new RatingAdapter(this, ratingsQuery) {
            @Override
            protected void onDataChanged() {
                if (getItemCount() == 0) {
                    reviewListRV.setVisibility(View.GONE);
                    reviewTotalCount.setText(R.string.no_review);
                    reviewTotalCount.setLetterSpacing((float) 0.1);
                    reviewRatingBar.setVisibility(View.GONE);
                    mEmptyView.setVisibility(View.VISIBLE);
                } else {
                    reviewListRV.setVisibility(View.VISIBLE);
                    reviewTotalCount.setVisibility(View.VISIBLE);
                    reviewRatingBar.setVisibility(View.VISIBLE);
                    mEmptyView.setVisibility(View.GONE);
                    if (newNumberRatings != 0 && newAvgRating != 0.0){
                        showRatingStatus(newNumberRatings , newAvgRating);
                    }
                }
            }
        };
        showRatingStatus(accommodationReviewNumber , accommodationReviewAvg);
        reviewListRV.setLayoutManager(new LinearLayoutManager(this));
        reviewListRV.setAdapter(mRatingAdapter);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(this, R.drawable.divider_style)));
        reviewListRV.addItemDecoration(itemDecoration);
        mRatingDialog = new RatingDialogFragment();

    }

    private void startSignIn() {
        Intent intent = new Intent(ReviewsActivity.this, SignUpActivity.class);
        startActivityForResult(intent, MainActivity.RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MainActivity.RC_SIGN_IN) {
            if (resultCode == RESULT_OK && FirebaseAuth.getInstance().getCurrentUser() != null) {
                mRatingDialog.show(getSupportFragmentManager(), RatingDialogFragment.TAG);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mRatingAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mRatingAdapter.stopListening();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.add_review_fab)
    public void onAddRatingClicked(View view) {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            startSignIn();
        } else {
            mRatingDialog.show(getSupportFragmentManager(), RatingDialogFragment.TAG);
        }
    }

    @Override
    public void onRating(RatingVO rating) {
        addRating(mAccommodationRef, rating)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        hideKeyboard();
                        reviewListRV.smoothScrollToPosition(0);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        hideKeyboard();
                        Snackbar.make(findViewById(android.R.id.content), "Failed to add rating",
                                Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    private void showRatingStatus(int numberRating , double avgRating){
        if (numberRating != 0){
            reviewTotalCount.setText(String.valueOf(numberRating).concat(" ").concat(getResources().getString(R.string.reviews)));
            reviewRatingBar.setRating((float) avgRating);
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)))
                    .hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private Task<Void> addRating(final DocumentReference documentReference, final RatingVO rating) {
        final DocumentReference ratingRef = documentReference.collection("ratings").document();
        return mFireStore.runTransaction(new Transaction.Function<Void>() {
            @Nullable
            @Override
            public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                AccommodationVO accommodation = transaction.get(documentReference).toObject(AccommodationVO.class);
                if (accommodation != null) {
                    newNumberRatings = accommodation.getNumRatings() + 1;

                    double oldRatingTotal = accommodation.getAvgRating() * accommodation.getNumRatings();
                    newAvgRating = (oldRatingTotal + rating.getRating()) / newNumberRatings;
                    accommodation.setNumRatings(newNumberRatings);
                    accommodation.setAvgRating(newAvgRating);
                    transaction.set(documentReference, accommodation);
                    transaction.set(ratingRef, rating);
                }
                return null;
            }
        });
    }
}
