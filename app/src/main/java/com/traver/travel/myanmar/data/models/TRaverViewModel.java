package com.traver.travel.myanmar.data.models;

import com.google.firebase.firestore.DocumentSnapshot;
import com.traver.travel.myanmar.data.vos.AccommodationVO;
import com.traver.travel.myanmar.data.vos.FiltersVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TRaverViewModel {
    private FiltersVO mFilters;

    private static TRaverViewModel instace;

    private HashMap<String, DocumentSnapshot> mHashmap;

    private TRaverViewModel() {
        mFilters = null;
        mHashmap = new HashMap<>();
    }

    public static TRaverViewModel getInstace() {
        if (instace == null) instace = new TRaverViewModel();
        return instace;
    }

    public void setSnapshots(List<DocumentSnapshot> snapshots) {
        mHashmap.clear();
        for (DocumentSnapshot snapshot : snapshots) {
            mHashmap.put(snapshot.getId(), snapshot);
        }
    }

    public List<DocumentSnapshot> getSnapshots(String city) {
        List<DocumentSnapshot> snapshots = new ArrayList<>();
        for (DocumentSnapshot snapshot : mHashmap.values()) {
            AccommodationVO accommodationVO = snapshot.toObject(AccommodationVO.class);
            if (accommodationVO != null && accommodationVO.getCity().equals(city)) {
                snapshots.add(snapshot);
            }
        }
        return snapshots;
    }

    public FiltersVO getFilters() {
        return mFilters;
    }

    public void setFilters(FiltersVO mFilters) {
        this.mFilters = mFilters;
    }

}
