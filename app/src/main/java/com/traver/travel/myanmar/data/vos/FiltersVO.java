package com.traver.travel.myanmar.data.vos;

import android.text.TextUtils;

import com.google.firebase.firestore.Query;

public class FiltersVO {

    private String accommodationCategory = null;
    private String accommodationCity = null;
    private String accommodationPlace = null;
    private double minPrice = -1;
    private double maxPrice = -1;
    private boolean partTime = false;
    private Query.Direction sortDirection = Query.Direction.DESCENDING;


    public FiltersVO() {
    }

    public boolean hasAccommodationCategory() {
        return !(TextUtils.isEmpty(accommodationCategory));
    }

    public boolean hasAccommodationCity() {
        return !(TextUtils.isEmpty(accommodationCity));
    }

    public boolean hasAccommodationPlace() {
        return !(TextUtils.isEmpty(accommodationPlace));
    }

    public boolean hasMinPrice() {
        return (minPrice >= 0);
    }

    public boolean hasMaxPrice() {
        return (maxPrice > 0);
    }

    public boolean hasSortDirection(){
        return sortDirection != null ;
    }

    public String getAccommodationCategory() {
        return accommodationCategory;
    }

    public void setAccommodationCategory(String accommodationCategory) {
        this.accommodationCategory = accommodationCategory;
    }

    public String getAccommodationCity() {
        return accommodationCity;
    }

    public void setAccommodationCity(String accommodationCity) {
        this.accommodationCity = accommodationCity;
    }

    public Query.Direction getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(Query.Direction sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getAccommodationPlace() {
        return accommodationPlace;
    }

    public void setAccommodationPlace(String accommodationPlace) {
        this.accommodationPlace = accommodationPlace;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public boolean getPartTime() {
        return partTime;
    }

    public void setPartTime(boolean partTime) {
        this.partTime = partTime;
    }

}
