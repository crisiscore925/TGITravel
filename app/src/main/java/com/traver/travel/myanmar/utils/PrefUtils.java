package com.traver.travel.myanmar.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.traver.travel.myanmar.data.vos.UserVO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class PrefUtils {

    private static final String ID_KEY = "userId";
    private static final String FAVOURITED_PROPERTIES = "favouritedPropertiesIds";

    private static SharedPreferences.Editor getPreferenceEditor(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.edit();
    }

    public static void setUser(Context context, UserVO user) {
        SharedPreferences.Editor editor = getPreferenceEditor(context);
        if (editor != null) {
            editor.putString(ID_KEY, user.getUserId());
            if (user.getUserFavList() != null && user.getUserFavList().size() > 0) {
                Set<String> ids = new HashSet<>(user.getUserFavList());
                editor.putStringSet(FAVOURITED_PROPERTIES, ids);
            }
            editor.apply();
        }
    }

    public static void removeUserData(Context context) {
        SharedPreferences.Editor editor = getPreferenceEditor(context);
        editor.clear().apply();

    }

    public static ArrayList<String> getUserBookmarks(Context context) {
        ArrayList<String> ids = new ArrayList<>();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> favouritedIds = sharedPreferences.getStringSet(FAVOURITED_PROPERTIES, null);
        if (favouritedIds != null && favouritedIds.size() > 0) {
            ids.addAll(favouritedIds);
        }
        return ids;
    }

    public static void addUserBookmarks(Context context, String id) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = getPreferenceEditor(context);
        Set<String> stringIds = sharedPreferences.getStringSet(FAVOURITED_PROPERTIES, null);
        if (stringIds == null) {
            stringIds = new HashSet<>();
        }
        stringIds.add(id);
        editor.putStringSet(FAVOURITED_PROPERTIES, stringIds);
        editor.apply();
    }

    public static void removeUserBookmarks(Context context, String id) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = getPreferenceEditor(context);
        Set<String> stringIds = sharedPreferences.getStringSet(FAVOURITED_PROPERTIES, null);
        if (stringIds == null) {
            return;
        }
        stringIds.remove(id);
        editor.putStringSet(FAVOURITED_PROPERTIES, stringIds);
        editor.apply();
    }

}
