package com.traver.travel.myanmar.activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import com.traver.travel.myanmar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UploadTimeActivity extends AppCompatActivity {

    @BindView(R.id.tp)
    TimePicker timePicker;
    @BindView(R.id.tv)
    TextView textView;
    private String time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }else {
            setTheme(R.style.AppThemeDark);
        }
        setContentView(R.layout.activity_upload_time);
        ButterKnife.bind(this);
        timePicker.setIs24HourView(true);
        time = getResources().getString(R.string.dummy_time);
        int i = getIntent().getIntExtra(UploadActivity.UPLOADTIME , -1);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
            if (i == UploadActivity.UPLOADCHECKIN){
                getSupportActionBar().setTitle(getString(R.string.check_in));
            }else if (i == UploadActivity.UPLOADCHECKOUT){
                getSupportActionBar().setTitle(getString(R.string.check_out));
            }
        }

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                if (minute < 10) {
                    time = String.valueOf(hour).concat(":").concat(String.valueOf(0)).concat(String.valueOf(minute));
                } else {
                    time = String.valueOf(hour).concat(":").concat(String.valueOf(minute));
                }
                textView.setText(time);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra(UploadActivity.UPLOADTIME, time);
            setResult(RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
