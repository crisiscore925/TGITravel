package com.traver.travel.myanmar.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.traver.travel.myanmar.data.vos.AccommodationVO;
import com.traver.travel.myanmar.R;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class SimilarAccommodationAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;

    private final List<DocumentSnapshot> mRecyclerViewItems;

    private SimilarAccommodationAdpater.OnAccommodationSelectedListener mListener;

    public SimilarAccommodationAdpater(Context context, SimilarAccommodationAdpater.OnAccommodationSelectedListener listener, List<DocumentSnapshot> recyclerViewItems) {
        this.mContext = context;
        this.mListener = listener;
        this.mRecyclerViewItems = recyclerViewItems;
    }

    public interface OnAccommodationSelectedListener {
        void onAccommodationSelected(DocumentSnapshot snapshot);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.similar_accommodation_card, parent, false);
        return new SimilarAccommodationAdpater.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.bind(mRecyclerViewItems.get(position));
    }

    @Override
    public int getItemCount() {
        if (mRecyclerViewItems.size() > 10) {
            return 10;
        } else {
            return mRecyclerViewItems.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.similar_image)
        ImageView similarImage;
        @BindView(R.id.similar_name)
        TextView similarName;
        @BindView(R.id.similar_rating)
        MaterialRatingBar similarRating;
        @BindView(R.id.similar_price)
        TextView similarPrice;
        @BindView(R.id.similar_location)
        TextView similarLocation;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onAccommodationSelected(mRecyclerViewItems.get(getAdapterPosition()));
                    }
                }
            });
        }


        public void bind(DocumentSnapshot snapshot) {
            if (snapshot != null) {
                AccommodationVO accommodation = snapshot.toObject(AccommodationVO.class);
                if (accommodation != null) {
                    if (accommodation.getPhotos().size() != 0) {
                        Glide.with(mContext)
                                .load(accommodation.getPhotos().get(0))
                                .into(similarImage);
                    } else {
                        Glide.with(mContext)
                                .load(R.drawable.ic_hotel_background_wide)
                                .into(similarImage);
                    }

                    similarName.setText(accommodation.getName());
                    similarRating.setRating((float) accommodation.getAvgRating());
                    String price;
                    if (accommodation.getCurrencyType().equals(mContext.getResources().getString(R.string.dollar))) {
                        price = mContext.getResources().getString(R.string.dollar_sign).concat(String.valueOf(accommodation.getMinPrice())).concat(" ").concat(mContext.getResources().getString(R.string.pernight));
                    } else {
                        price = String.valueOf(accommodation.getMinPrice()).concat(" ").concat(mContext.getResources().getString(R.string.kyats)).concat(" ").concat(mContext.getResources().getString(R.string.pernight));
                    }
                    similarPrice.setText(price);
                    String location;
                    if (accommodation.getPlace().equals(mContext.getResources().getString(R.string.anywhere))) {
                        location = accommodation.getCity();
                    } else {
                        location = accommodation.getPlace().concat(" , ").concat(accommodation.getCity());
                    }
                    similarLocation.setText(location);
                }
            }

        }
    }
}
