package com.traver.travel.myanmar.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.traver.travel.myanmar.fragments.ExploreFragment;
import com.traver.travel.myanmar.R;


public class HotelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private HotelAdapterOnClickHandler clickHandler;
    private String[] names;
    private int[] resources;
    private int viewType;

    public HotelAdapter(Context mContext, HotelAdapterOnClickHandler clickHandler, String[] names, int[] resources, int viewType) {
        this.mContext = mContext;
        this.clickHandler = clickHandler;
        this.names = names;
        this.resources = resources;
        this.viewType = viewType;
    }

    public interface HotelAdapterOnClickHandler {
        void onClick(int position , int viewType);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ExploreFragment.VIEW_BY_TYPE:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_card_view, parent, false);
                return new HotelAdapter.MyViewHolder(view);

            case ExploreFragment.VIEW_BY_PLACE:
                View viewByPlace = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_card_view, parent, false);
                return new HotelAdapter.PlacesViewHolder(viewByPlace);

            default:
                View viewDefault = LayoutInflater.from(parent.getContext()).inflate(R.layout.hotel_card_view, parent, false);
                return new HotelAdapter.MyViewHolder(viewDefault);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int Res = resources[position];
        String label = names[position];
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ExploreFragment.VIEW_BY_TYPE:
                MyViewHolder myViewHolder = (MyViewHolder) holder;
                Glide.with(mContext)
                        .load(Res)
                        .into(myViewHolder.imageView);

                myViewHolder.textView.setText(label);
                break;
            case ExploreFragment.VIEW_BY_PLACE:
                PlacesViewHolder placesViewHolder = (PlacesViewHolder) holder;
                Glide.with(mContext)
                        .load(Res)
                        .into(placesViewHolder.placeImageView);

                placesViewHolder.placeLabelView.setText(label);
                break;
        }
    }

    public class PlacesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView placeImageView;
        TextView placeLabelView;

        PlacesViewHolder(View itemView) {
            super(itemView);
            placeImageView = itemView.findViewById(R.id.place_image);
            placeLabelView = itemView.findViewById(R.id.place_label);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickHandler == null) {
                return;
            }
            clickHandler.onClick(getAdapterPosition() , getItemViewType());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewType == ExploreFragment.VIEW_BY_TYPE){
            return ExploreFragment.VIEW_BY_TYPE;
        }else {
            return ExploreFragment.VIEW_BY_PLACE;
        }
    }

    @Override
    public int getItemCount() {
        return names.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        TextView textView;

        MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickHandler == null) {
                return;
            }
            clickHandler.onClick(getAdapterPosition() , getItemViewType());
        }
    }
}
