package com.traver.travel.myanmar.activities;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.TRaveApp;
import com.traver.travel.myanmar.data.vos.UserVO;
import com.traver.travel.myanmar.fragments.ExperienceFragment;
import com.traver.travel.myanmar.fragments.ExploreFragment;
import com.traver.travel.myanmar.fragments.FilterFragment;
import com.traver.travel.myanmar.fragments.ProfileFragment;
import com.traver.travel.myanmar.fragments.SavedFragment;
import com.traver.travel.myanmar.utils.BottomNavigationViewHelper;
import com.traver.travel.myanmar.utils.ConnectivityReceiver;
import com.traver.travel.myanmar.utils.PrefUtils;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements ExploreFragment.SearchButtonClickListener, SavedFragment.StartExploreClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    public static final int RC_SIGN_IN = 9001;
    private BottomNavigationView navigation;
    private boolean exit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            setTheme(R.style.AppThemeDark_NoActionBar);
        }
        setContentView(R.layout.activity_main);
        FirebaseApp.initializeApp(this);
        FirebaseFirestore.setLoggingEnabled(true);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, ExploreFragment.newInstance());
        transaction.commit();
        navigation = findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TRaveApp.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            final Dialog dialog = new Dialog(this);
            if (dialog.getWindow() != null) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.exit_layout);
            Button exit = dialog.findViewById(R.id.exit);
            Button cancel = dialog.findViewById(R.id.cancel);
            exit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (FirebaseAuth.getInstance().getCurrentUser() != null && ConnectivityReceiver.isConnected()) {
                        updateUser(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
                    } else {
                        MainActivity.super.finish();
                    }
                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (view != null) {
                        dialog.dismiss();
                    }
                }
            });
            dialog.show();
        } else {
            navigation.setSelectedItemId(R.id.navigation_home);
            exit = true;
        }
    }

    private void updateUser(FirebaseUser firebaseUser) {
        UserVO user = new UserVO();
        user.setUserId(firebaseUser.getUid());
        ArrayList<String> favList;
        favList = PrefUtils.getUserBookmarks(this);
        user.setUserFavList(favList);
        CollectionReference collectionReference = FirebaseFirestore.getInstance().collection(UserVO.UPLOAD_USER);
        collectionReference.document(firebaseUser.getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                FirebaseAuth.getInstance().signOut();
                PrefUtils.removeUserData(MainActivity.this);
                MainActivity.super.onBackPressed();
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (aVoid != null){
                    FirebaseAuth.getInstance().signOut();
                    PrefUtils.removeUserData(MainActivity.this);
                    MainActivity.super.onBackPressed();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainActivity.this, e.getMessage() , Toast.LENGTH_SHORT).show();
                FirebaseAuth.getInstance().signOut();
                PrefUtils.removeUserData(MainActivity.this);
                MainActivity.super.onBackPressed();
            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment Fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Fragment = ExploreFragment.newInstance();
                    exit = true;
                    break;

                case R.id.navigation_search:
                    Fragment = new FilterFragment();
                    exit = false;
                    break;

                case R.id.navigation_places:
                    Fragment = ExperienceFragment.newInstance();
                    exit = false;
                    break;

                case R.id.navigation_saved:
                    Fragment = SavedFragment.newInstance();
                    exit = false;
                    break;

                case R.id.navigation_profile:
                    Fragment = ProfileFragment.newInstance();
                    exit = false;
                    break;

                default:
                    Fragment = ExploreFragment.newInstance();
                    exit = false;
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, Fragment);
            transaction.commit();
            return true;
        }
    };

    @Override
    public void onButtonClicked() {
        navigation.setSelectedItemId(R.id.navigation_search);
    }

    @Override
    public void onExploreClicked() {
        navigation.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
