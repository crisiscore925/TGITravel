package com.traver.travel.myanmar.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.traver.travel.myanmar.R;

import java.util.List;

public class AmenitiesListAdapter extends RecyclerView.Adapter<AmenitiesListAdapter.ViewHOlder> {

    private Context mContext;
    private List<Integer> mAmenities;

    public AmenitiesListAdapter(Context context, List<Integer> amenities) {
        this.mContext = context;
        this.mAmenities = amenities;
    }

    @NonNull
    @Override
    public AmenitiesListAdapter.ViewHOlder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.amenities_item_list_view, parent, false);
        return new ViewHOlder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AmenitiesListAdapter.ViewHOlder holder, int position) {
        int resID;
        int resName;
        switch (mAmenities.get(position)) {
            case 1:
                resID = R.drawable.ic_wifi_black_24dp;
                resName = R.string.upload_wifi;
                break;

            case 2:
                resID = R.drawable.ic_pool_black_24dp;
                resName = R.string.upload_pool;
                break;

            case 3:
                resID = R.drawable.ic_spa_black_24dp;
                resName = R.string.upload_spa;
                break;

            case 4:
                resID = R.drawable.ic_restaurant_menu_black_24dp;
                resName = R.string.upload_restaurants;
                break;

            case 5:
                resID = R.drawable.ic_local_bar_black_24dp;
                resName = R.string.upload_bar;
                break;

            case 6:
                resID = R.drawable.ic_local_laundry_service_black_24dp;
                resName = R.string.upload_laundry;
                break;

            case 7:
                resID = R.drawable.ic_tv_black_24dp;
                resName = R.string.upload_tv;
                break;

            case 8:
                resID = R.drawable.ic_local_parking_black_24dp;
                resName = R.string.upload_car_park;
                break;

            case 9:
                resID = R.drawable.ic_airport_shuttle_black_24dp;
                resName = R.string.upload_shuttle;
                break;

            case 10:
                resID = R.drawable.ic_ac_unit_black_24dp;
                resName = R.string.upload_air_con;
                break;

            case 11:
                resID = R.drawable.ic_phone_in_talk_black_24dp;
                resName = R.string.upload_telephone;
                break;

            case 12:
                resID = R.drawable.ic_bathtub_with_opened_shower;
                resName = R.string.upload_shower;
                break;

            case 13:
                resID = R.drawable.ic_toilet;
                resName = R.string.upload_toilet;
                break;

            case 14:
                resID = R.drawable.ic_refrigerator;
                resName = R.string.upload_refrigerator;
                break;

            case 15:
                resID = R.drawable.ic_money_off_black_24dp;
                resName = R.string.discount;
                break;

            default:
                resID = R.mipmap.ic_launcher;
                resName = R.string.app_name;

        }
        holder.amenitiesName.setText(mContext.getResources().getString(resName));
        holder.amenitiesImage.setImageResource(resID);
    }

    @Override
    public int getItemCount() {
        return mAmenities.size();
    }

    class ViewHOlder extends RecyclerView.ViewHolder {
        TextView amenitiesName;
        ImageView amenitiesImage;

        ViewHOlder(View itemView) {
            super(itemView);
            amenitiesImage = itemView.findViewById(R.id.amenities_list_icon);
            amenitiesName = itemView.findViewById(R.id.amenities_list_name);
        }
    }
}
