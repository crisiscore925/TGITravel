package com.traver.travel.myanmar.activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.traver.travel.myanmar.R;

public class UploadMapActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Marker marker;
    private LatLng latLngData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }else {
            setTheme(R.style.AppThemeDark);
        }
        setContentView(R.layout.activity_upload_map);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.upload_map);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
        }

        Bundle bundle = getIntent().getParcelableExtra("LocationVO");
        if (bundle != null){
            latLngData = bundle.getParcelable("LatLang");
        }


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.upload_map_fragment);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (mMap == null || !mMap.equals(googleMap)) {
            mMap = googleMap;
            if (marker == null  && latLngData == null) {
                latLngData = new LatLng(17.150522, 95.986873);
                marker = mMap.addMarker(new MarkerOptions().position(latLngData).draggable(true));
                moveTo(CameraPosition.builder().target(latLngData).zoom(5).build());
            }else if (latLngData != null){
                marker = mMap.addMarker(new MarkerOptions().position(latLngData).draggable(true));
                moveTo(CameraPosition.builder().target(latLngData).zoom(13).build());
            }
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    setMarker(latLng);
                    latLngData = latLng;
                }
            });
        }
    }

    private void setMarker(LatLng latLng) {
        if (marker != null) {
            marker.remove();
        }
        marker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        moveTo(CameraPosition.builder().target(latLng).zoom(15).build());
    }

    private void moveTo(CameraPosition cp) {
        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp), 1000, null);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            backToUpload(latLngData);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        backToUpload(latLngData);
    }

    private void backToUpload(LatLng latLngData) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("LocationVO" , latLngData);
        Intent intent = new Intent(UploadMapActivity.this, UploadActivity.class);
        intent.putExtra("LocationVO", bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

}
