package com.traver.travel.myanmar.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public abstract class FireStoreAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> implements com.google.firebase.firestore.EventListener<QuerySnapshot> {

    private static final String TAG = "Firestore Adapter";
    private Query mQuery;
    private ListenerRegistration mRegistration;
    private ArrayList<DocumentSnapshot> mSnapshots = new ArrayList<>();

    FireStoreAdapter(Query query) {
        mQuery = query;
    }

    public void startListening(){
        if (mQuery != null && mRegistration == null) {
            mRegistration = mQuery.addSnapshotListener(this);
        }
    }

    public void stopListening() {
        if (mRegistration != null) {
            mRegistration.remove();
            mRegistration = null;
        }

        mSnapshots.clear();
        notifyDataSetChanged();
    }

    public void setQuery(Query query) {
        // Stop listening
        stopListening();

        // Clear existinkodig data
        mSnapshots.clear();
        notifyDataSetChanged();

        // Listen to new query
        mQuery = query;
        startListening();
    }

    @Override
    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
        if (e != null) {
            Log.w(TAG, "onEventError", e);
            return;
        }
        for (DocumentChange change : documentSnapshots.getDocumentChanges()) {
            switch (change.getType()) {
                case ADDED:
                    onDocumentAdded(change);
                    break;

                case MODIFIED:
                    onDocumentModified(change);
                    break;

                case REMOVED:
                    onDocumentRemoved(change);
                    break;
            }
        }
        onDataChanged();
    }

    private void onDocumentAdded(DocumentChange change){
        mSnapshots.add(change.getNewIndex() , change.getDocument());
        notifyItemInserted(change.getNewIndex());
    }

    private void onDocumentModified(DocumentChange change){
        if (change.getOldIndex() == change.getNewIndex()){
            mSnapshots.set(change.getOldIndex() ,change.getDocument());
            notifyItemChanged(change.getOldIndex());
        }else {
            mSnapshots.remove(change.getOldIndex());
            mSnapshots.add(change.getNewIndex() , change.getDocument());
            notifyItemMoved(change.getOldIndex() , change.getNewIndex());
        }
    }

    private void onDocumentRemoved(DocumentChange change){
        mSnapshots.remove(change.getOldIndex());
        notifyItemRemoved(change.getOldIndex());
    }

    @Override
    public int getItemCount() {
        return mSnapshots.size();
    }

    DocumentSnapshot getSnapshot(int index) {
        return mSnapshots.get(index);
    }

    public ArrayList<DocumentSnapshot> getSnapshots(){
        return mSnapshots;
    }

    protected void onError(FirebaseFirestoreException e) {

    }

    protected void onDataChanged() {

    }

}
