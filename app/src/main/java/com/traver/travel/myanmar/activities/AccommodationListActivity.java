package com.traver.travel.myanmar.activities;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.adapters.AccommodationListAdapter;
import com.traver.travel.myanmar.data.models.TRaverViewModel;
import com.traver.travel.myanmar.data.vos.AccommodationVO;
import com.traver.travel.myanmar.data.vos.FiltersVO;
import com.traver.travel.myanmar.data.vos.UserVO;
import com.traver.travel.myanmar.utils.PrefUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccommodationListActivity extends AppCompatActivity implements AccommodationListAdapter.OnAccommodationSelectedListener {

    private static final String TAG = "AccommodationActivity";
    public static final int RC_SIGN_IN = 9002;
    @BindView(R.id.accommodation_list)
    RecyclerView accommodationList;
    @BindView(R.id.error_layout)
    RelativeLayout errorLayout;
    @BindView(R.id.list_loading)AVLoadingIndicatorView loading;

    private AccommodationListAdapter mAdapter;
    private Query mQuery;
    public static final String QUERYID = "QueryID";
    public static final int HOTELQUERYID = 1;
    public static final int RESORTQUERYID = 2;
    public static final int MOTELQUERYID = 3;
    public static final int HOSTELQUERYID = 4;
    public static final int GUESTHOUSEQUERYID = 5;
    public static final int INNQUERYID = 6;
    public static final int YANGONQUERYID = 7;
    public static final int MANDALAYQUERYID = 8;
    public static final int BAGANQUERYID = 9;
    public static final int KYAIKHTIYOEQUERYID = 10;
    public static final int HPA_ANQUERYID = 11;
    public static final int BAGOQUERYID = 12;
    public static final int KALAWQUERYID = 13;
    public static final int HSIPAWQUERYID = 14;
    public static final int TAUNGGYIQUERYID = 15;
    public static final int INLEQUERYID = 16;
    public static final int PYINOOLWINQUERYID = 17;
    public static final int PYAYQUERYID = 18;
    public static final int NAWBUBAWQUERYID = 19;
    public static final int GAWYANGYIQUERYID = 20;
    public static final int CHAUNGTHAQUERYID = 21;
    public static final int NGWESAUNGQUERYID = 22;
    public static final int NGAPALIQUERYID = 23;
    public static final int KAWTHOUNGQUERYID = 24;
    public static final int MYAIKISLANDQUERYID = 25;
    public static final int FILTERQUERYID = 26;
    private ArrayList<DocumentSnapshot> mRecyclerViewItems = new ArrayList<>();
    private FirebaseFirestore mFireStore;
    private boolean isQueryData = false;
    private CollectionReference collectionReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            setTheme(R.style.AppThemeDark);
        }
        setContentView(R.layout.activity_accommodation_list);

        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(6);
        }
        setTitle(R.string.loading);
        loading.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
        initFirestore();
        initRecyclerView();
        accommodationList.setLayoutManager(new LinearLayoutManager(this));
        accommodationList.setAdapter(mAdapter);
    }

    private void initFirestore() {
        mFireStore = FirebaseFirestore.getInstance();
        mQuery = mFireStore.collection(UploadActivity.UPLOADREF);
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        mFireStore.setFirestoreSettings(settings);
        collectionReference = mFireStore.collection(UserVO.UPLOAD_USER);
    }

    private void initRecyclerView() {
        if (mQuery == null) {
            Log.w(TAG, "No query, not initializing RecyclerView");
        }
        mAdapter = new AccommodationListAdapter(this, this , mRecyclerViewItems , mQuery) {
            @Override
            protected void onDataChanged() {
                if (!isQueryData) {
                    setQueryData();
                } else {
                    setDataView();
                }
            }

            @Override
            protected void onError(FirebaseFirestoreException e) {
                Snackbar.make(findViewById(android.R.id.content),
                        "Something went wrong!", Snackbar.LENGTH_SHORT).show();
            }
        };
        mAdapter.setQuery(mQuery);
    }

    public void setDataView() {
        loading.setVisibility(View.GONE);
        if (mAdapter.getSnapshots().size() == 0) {
            accommodationList.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
        } else {
            accommodationList.setVisibility(View.VISIBLE);
            errorLayout.setVisibility(View.GONE);
        }
        mRecyclerViewItems.addAll(mAdapter.getSnapshots());
        TRaverViewModel.getInstace().setSnapshots(mAdapter.getSnapshots());
    }

    public void setQueryData() {
        isQueryData = true;
        String title;
        int queryID = getIntent().getIntExtra(QUERYID, -1);
        switch (queryID) {
            case HOTELQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CATEGORY, getResources().getString(R.string.hotel)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                setTitle(R.string.hotels);
                break;

            case RESORTQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CATEGORY, getResources().getString(R.string.resort)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                setTitle(R.string.resorts);
                break;

            case MOTELQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CATEGORY, getResources().getString(R.string.motel)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                setTitle(R.string.motels);
                break;

            case HOSTELQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CATEGORY, getResources().getString(R.string.hostel)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                setTitle(R.string.hostels);
                break;

            case GUESTHOUSEQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CATEGORY, getResources().getString(R.string.guesthouse)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                setTitle(R.string.guesthouse);
                break;

            case INNQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CATEGORY, getResources().getString(R.string.inn)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                setTitle(R.string.inn);
                break;

            case YANGONQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.yangon)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.yangon));
                setTitle(title);
                break;

            case MANDALAYQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.mandalay)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.mandalay));
                setTitle(title);
                break;

            case BAGANQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.bagan)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.bagan));
                setTitle(title);
                break;

            case KYAIKHTIYOEQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.kyaik_hti_yoe)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.kyaik_hti_yoe));
                setTitle(title);
                break;

            case HPA_ANQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.hpa_an)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.hpa_an));
                setTitle(title);
                break;

            case BAGOQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.bago)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.bago));
                setTitle(title);
                break;

            case KALAWQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.kalaw)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.kalaw));
                setTitle(title);
                break;

            case HSIPAWQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.hsipaw)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.hsipaw));
                setTitle(title);
                break;

            case TAUNGGYIQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.taung_gyi)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.taung_gyi));
                setTitle(title);
                break;

            case INLEQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.inle)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.inle));
                setTitle(title);
                break;

            case PYINOOLWINQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.pyin_oo_lwin)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.pyin_oo_lwin));
                setTitle(title);
                break;

            case PYAYQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.pyay)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.pyay));
                setTitle(title);
                break;

            case NAWBUBAWQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.naw_bu_baw)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.naw_bu_baw));
                setTitle(title);
                break;

            case GAWYANGYIQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.gaw_yan_gyi)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.gaw_yan_gyi));
                setTitle(title);
                break;

            case CHAUNGTHAQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.chaung_tha)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.chaung_tha));
                setTitle(title);
                break;

            case NGWESAUNGQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.ngwe_saung)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.ngwe_saung));
                setTitle(title);
                break;

            case NGAPALIQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.ngapali)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.ngapali));
                setTitle(title);
                break;

            case KAWTHOUNGQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.kaw_thoung)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.kaw_thoung));
                setTitle(title);
                break;

            case MYAIKISLANDQUERYID:
                mQuery = mQuery.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, getResources().getString(R.string.myaik)).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
                title = getResources().getString(R.string.accommodations_in).concat(" ").concat(getResources().getString(R.string.myaik));
                setTitle(title);
                break;

            case FILTERQUERYID:
                FiltersVO filters = getFilter();
                mQuery = getFilterQuery(mQuery, filters);
                setTitle(getResources().getString(R.string.filter));
                break;

            default:
                setTitle(R.string.loading);
                break;
        }
        this.mQuery = mQuery;
        mAdapter.setQuery(this.mQuery);
        if (queryID == FILTERQUERYID) showDelayEmpty();

    }

    private void showDelayEmpty() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mRecyclerViewItems.size() == 0) {
                    loading.setVisibility(View.GONE);
                    errorLayout.setVisibility(View.VISIBLE);
                }
            }
        } , 30000);
    }

    private FiltersVO getFilter() {
        FiltersVO filters = new FiltersVO();
        filters.setAccommodationCategory(getIntent().getStringExtra(AccommodationVO.ACCOMMODATION_CATEGORY));
        filters.setAccommodationCity(getIntent().getStringExtra(AccommodationVO.ACCOMMODATION_CITY));
        filters.setAccommodationPlace(getIntent().getStringExtra(AccommodationVO.ACCOMMODATION_PLACE));
        filters.setMinPrice(getIntent().getDoubleExtra(AccommodationVO.ACCOMMODATION_MIN_PRICE, -1));
        filters.setMaxPrice(getIntent().getDoubleExtra(AccommodationVO.ACCOMMODATION_MAX_PRICE, -1));
        filters.setPartTime(getIntent().getBooleanExtra(AccommodationVO.ACCOMMODATION_PART_TIME, false));
        return filters;
    }

    private Query getFilterQuery(Query query, FiltersVO filters) {

        if (filters.hasAccommodationCategory()) {
            query = query.whereEqualTo(AccommodationVO.ACCOMMODATION_CATEGORY, filters.getAccommodationCategory());
        }
        if (filters.hasAccommodationCity()) {
            query = query.whereEqualTo(AccommodationVO.ACCOMMODATION_CITY, filters.getAccommodationCity());
        }
        if (filters.hasAccommodationPlace()) {
            query = query.whereEqualTo(AccommodationVO.ACCOMMODATION_PLACE, filters.getAccommodationPlace());
        }
        if (filters.hasMinPrice()) {
            query = query.whereGreaterThan(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, filters.getMinPrice()).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
        }
        if (filters.hasMaxPrice()) {
            query = query.whereLessThan(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, filters.getMaxPrice()).orderBy(AccommodationVO.ACCOMMODATION_CALCULATED_PRICE, Query.Direction.DESCENDING).orderBy(AccommodationVO.ACCOMMODATION_AVG_RATING, Query.Direction.DESCENDING);
        }
        if (filters.getPartTime()) {
            query = query.whereEqualTo(AccommodationVO.ACCOMMODATION_PART_TIME, filters.getPartTime());
        }
        return query;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAccommodationSelected(final DocumentSnapshot snapshot) {
        Intent intent = new Intent(AccommodationListActivity.this, AccommodationInfoActivity.class);
        intent.putExtra(AccommodationInfoActivity.KEY_ACCOMMODATION_ID, snapshot.getId());
        startActivity(intent);
    }

    @Override
    public void onAccommodationFavourited(String snapshotID) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            addFavourite(snapshotID);
        } else {
            startSignIn();
        }
    }

    @Override
    public void onAccommodationRemoved(String snapshotID, int position) {

    }

    private void addFavourite(String snapshotID) {
        if (PrefUtils.getUserBookmarks(this).contains(snapshotID)) {
            PrefUtils.removeUserBookmarks(this, snapshotID);
        } else {
            PrefUtils.addUserBookmarks(this, snapshotID);
        }
        updateUser(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
    }

    private void updateUser(FirebaseUser firebaseUser) {
        UserVO user = new UserVO();
        user.setUserId(firebaseUser.getUid());
        ArrayList<String> favList;
        favList = PrefUtils.getUserBookmarks(this);
        user.setUserFavList(favList);
        CollectionReference collectionReference = FirebaseFirestore.getInstance().collection(UserVO.UPLOAD_USER);
        collectionReference.document(firebaseUser.getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (aVoid != null) {
                    Toast.makeText(AccommodationListActivity.this, "Successfully added to your favourites", Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AccommodationListActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AccommodationListActivity.RC_SIGN_IN) {
            if (resultCode == RESULT_OK && FirebaseAuth.getInstance().getCurrentUser() != null) {
                Toast.makeText(this, "Welcome", Toast.LENGTH_SHORT).show();
                checkUser();
            }
        }
    }

    void checkUser() {
        DocumentReference documentReference = collectionReference.document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
        documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot snapshot) {
                if (snapshot.exists()) {
                    PrefUtils.setUser(AccommodationListActivity.this, snapshot.toObject(UserVO.class));
                } else {
                    uploadUser(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
                }
            }
        });
    }

    private void uploadUser(FirebaseUser firebaseUser) {
        final UserVO user = new UserVO();
        user.setUserId(firebaseUser.getUid());
        ArrayList<String> favList = new ArrayList<>();
        user.setUserFavList(favList);
        collectionReference.document(firebaseUser.getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                PrefUtils.setUser(AccommodationListActivity.this, user);
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                PrefUtils.setUser(AccommodationListActivity.this, user);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AccommodationListActivity.this, "UserVO Upload Failed", Toast.LENGTH_SHORT).show();
                PrefUtils.setUser(AccommodationListActivity.this, user);
            }
        });
    }

    private void startSignIn() {
        Intent intent = new Intent(AccommodationListActivity.this, SignUpActivity.class);
        startActivityForResult(intent, AccommodationListActivity.RC_SIGN_IN);
    }
}
