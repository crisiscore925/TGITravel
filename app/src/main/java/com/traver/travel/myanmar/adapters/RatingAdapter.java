package com.traver.travel.myanmar.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.firestore.Query;
import com.traver.travel.myanmar.data.vos.RatingVO;
import com.traver.travel.myanmar.R;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class RatingAdapter extends FireStoreAdapter<RatingAdapter.ViewHolder> {
    private Context mContext;


    protected RatingAdapter(Context context, Query query) {
        super( query);
        this.mContext = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.review_photo)
        ImageView reviewPhoto;
        @BindView(R.id.review_name)
        TextView reviewName;
        @BindView(R.id.review_rating)
        MaterialRatingBar reviewRating;
        @BindView(R.id.review_body)
        TextView reviewBody;
        @BindView(R.id.review_timestamp)
        TextView reviewTimeStamp;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(RatingVO rating) {
            reviewName.setText(rating.getUserName());
            reviewRating.setRating((float) rating.getRating());
            reviewBody.setText(rating.getText());
            Date date = rating.getTimestamp();
            String month = (String) DateFormat.format("MMM",  date);
            String year = (String)DateFormat.format("yyyy" , date);
            String reviewDate = month.concat(" ").concat(year);
            reviewTimeStamp.setText(reviewDate);
            if (rating.getUserPhoto() != null) {
                Glide.with(mContext)
                        .load(rating.getUserPhoto())
                        .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                        .into(reviewPhoto);
            } else {
                Glide.with(mContext).load(R.drawable.profile_dummy)
                .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                .into(reviewPhoto);
            }
        }
    }

    @Override
    public int getItemCount() {
        return super.getItemCount();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_rating, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(getSnapshot(position).toObject(RatingVO.class));
    }

}
