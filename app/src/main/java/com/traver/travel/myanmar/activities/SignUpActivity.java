package com.traver.travel.myanmar.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.utils.ConnectivityReceiver;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Arrays;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private static final String TAG = "Google Sign In Error";

    @BindView(R.id.account_label)
    TextView accountLabel;
    @BindView(R.id.email_sign_in)
    EditText emailField;
    @BindView(R.id.password_sign_in)
    EditText passwordField;
    @BindView(R.id.btn_sign_in)
    Button signInButton;
    @BindView(R.id.change_sign_in_up)
    Button changeSignInUpBtn;
    @BindView(R.id.facebook_button_sign_in)
    FloatingActionButton facebookSignIn;
    @BindView(R.id.google_button_sign_in)
    FloatingActionButton googleSignIn;

    private boolean isSignInForm = true;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 1;
    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            setTheme(R.style.AppThemeDark);
        }
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));

        }

        mAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.loading_dialog);
        dialog.setCancelable(true);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        facebookSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityReceiver.isConnected()) {
                    dialog.show();
                    LoginManager loginManager = LoginManager.getInstance();
                    loginManager.logInWithReadPermissions(SignUpActivity.this, Arrays.asList("email", "public_profile"));
                    LoginManager.getInstance().registerCallback(callbackManager,
                            new FacebookCallback<LoginResult>() {
                                @Override
                                public void onSuccess(LoginResult loginResult) {
                                    if (loginResult.getAccessToken() != null) {
                                        handleFacebookAccessToken(loginResult.getAccessToken());
                                    }
                                }

                                @Override
                                public void onCancel() {
                                    showSnackBar("Facebook Sign In Cancel");
                                }

                                @Override
                                public void onError(FacebookException exception) {
                                    showSnackBar(exception.getMessage());
                                }
                            });
                } else {
                    showSnackBar(getResources().getString(R.string.check_connection));
                }
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityReceiver.isConnected()) {
                    dialog.show();
                    googleSignIn();
                } else {
                    showSnackBar(getResources().getString(R.string.check_connection));
                }
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConnectivityReceiver.isConnected()) {
                    if (isSignInForm) {
                        signInWithEmail();
                    } else {
                        signUpWithEmail();
                    }
                } else {
                    showSnackBar(getResources().getString(R.string.check_connection));
                }
            }
        });

        showSignInUPView(isSignInForm);
        changeSignInUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSignInForm) {
                    showSignInUPView(false);
                    isSignInForm = false;
                } else {
                    showSignInUPView(true);
                    isSignInForm = true;
                }
            }
        });
    }

    private void showSnackBar(String message) {
        dialog.hide();
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(R.color.colorWhite));
        snackbar.show();
    }


    private void signUpWithEmail() {
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();

        if (email.equals("") || password.equals("")) {
            Toast.makeText(this, "Fill all field", Toast.LENGTH_SHORT).show();
        } else {
            dialog.show();
            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        showSnackBar(Objects.requireNonNull(task.getException()).getMessage());

                    }
                }
            });
        }
    }

    private void signInWithEmail() {
        String email = emailField.getText().toString();
        String password = passwordField.getText().toString();
        if (email.equals("") || password.equals("")) {
            Toast.makeText(this, "Fill all field", Toast.LENGTH_SHORT).show();
        } else {
            dialog.show();
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        showSnackBar(Objects.requireNonNull(task.getException()).getMessage());
                    }
                }
            });
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            showSnackBar(Objects.requireNonNull(task.getException()).getMessage());
                        }

                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            dialog.hide();
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void showSignInUPView(boolean isSignInForm) {
        if (isSignInForm) {
            showSignInView();
        } else {
            showSignUpView();
        }
    }

    private void showSignInView() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.sign_in);
        }
        signInButton.setText(R.string.sign_in);
        accountLabel.setText(R.string.create_account);
        changeSignInUpBtn.setText(R.string.sign_up);
        emailField.requestFocus();
    }

    private void showSignUpView() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.sign_up);
        }
        accountLabel.setText(R.string.have_account);
        signInButton.setText(R.string.sign_up);
        changeSignInUpBtn.setText(R.string.sign_in);
    }

    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
                showSnackBar(e.getMessage());
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            showSnackBar(Objects.requireNonNull(task.getException()).getMessage());
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
