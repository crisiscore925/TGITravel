package com.traver.travel.myanmar.activities;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.adapters.UploadImageAdapter;
import com.traver.travel.myanmar.data.vos.AccommodationVO;
import com.traver.travel.myanmar.data.vos.LocationVO;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UploadActivity extends AppCompatActivity implements EventListener<DocumentSnapshot> {

    private static final int RESULT_LOAD_IMAGE1 = 1;
    public static final int RESULT_LOAD_MAP = 2;
    private List<String> fileNameList;
    private List<String> fileDoneList;
    private UploadImageAdapter uploadImageAdapter;
    private StorageReference mStroage;
    public static final String UPLOADDONE = "DONE";
    public static final String UPLOADFAILED = "FAILED";
    public static final String UPLOADING = "UPLOADING";
    public static final String UPLOADREF = "Accommodations";
    public static final String UPLOADTIME = "UploadTime";
    public static final int UPLOADCHECKIN = 101;
    public static final int UPLOADCHECKOUT = 102;
    public List<String> uploadedImages;
    private FirebaseFirestore mFirestore;
    private ListenerRegistration mAccommodationRegistration;
    private boolean isUpdate;
    private AccommodationVO loadedAccommodation;

    @BindView(R.id.upload_images_rv)
    RecyclerView uploadImagesRV;
    @BindView(R.id.upload_images_btn)
    Button uploadImageButton;
    @BindView(R.id.upload_name)
    EditText nameField;
    @BindView(R.id.phone1)
    EditText phoneOneField;
    @BindView(R.id.phone2)
    EditText phoneTwoField;
    @BindView(R.id.phone3)
    EditText phoneThreeField;
    @BindView(R.id.website)
    EditText websiteField;
    @BindView(R.id.city_spinner)
    Spinner citySpinner;
    @BindView(R.id.place_spinner)
    Spinner placeSpinner;
    @BindView(R.id.accommodation_spinner)
    Spinner accommodationSpinner;
    @BindView(R.id.upload_min_price)
    EditText minPriceField;
    @BindView(R.id.upload_max_price)
    EditText maxPriceField;
    @BindView(R.id.currency_type)
    Spinner currencySpinner;
    @BindView(R.id.upload_wifi_switch)
    Switch wifiSwitch;
    @BindView(R.id.upload_pool_switch)
    Switch poolSwitch;
    @BindView(R.id.upload_spa_switch)
    Switch spaSwitch;
    @BindView(R.id.upload_restaurants_switch)
    Switch restaurantSwitch;
    @BindView(R.id.upload_bar_switch)
    Switch barSwitch;
    @BindView(R.id.upload_laundry_switch)
    Switch laundrySwitch;
    @BindView(R.id.upload_tv_switch)
    Switch tvSwitch;
    @BindView(R.id.upload_car_park_switch)
    Switch carParkingSwitch;
    @BindView(R.id.upload_shuttle_switch)
    Switch shuttleSwitch;
    @BindView(R.id.upload_air_con_switch)
    Switch airConSwitch;
    @BindView(R.id.upload_telephone_switch)
    Switch telephoneSwitch;
    @BindView(R.id.upload_shower_switch)
    Switch showerSwitch;
    @BindView(R.id.upload_toilet_switch)
    Switch toiletSwitch;
    @BindView(R.id.upload_refrigerator_switch)
    Switch refrigeratorSwitch;
    @BindView(R.id.upload_discount_switch)
    Switch discountSwitch;
    @BindView(R.id.upload_part_time_switch)
    Switch partTimeSwitch;
    private List<Integer> listAmenities;
    @BindView(R.id.upload_map_btn)
    Button uploadMapButton;
    @BindView(R.id.check_in_time)
    TextView checkInTime;
    @BindView(R.id.check_out_time)
    TextView checkOutTime;
    @BindView(R.id.upload_address_edittext)
    EditText uploadAddressField;
    @BindView(R.id.upload_part_time_price)
    EditText uploadPartTimePriceField;
    @BindView(R.id.partime_price_layout)
    LinearLayout partTimePriceLayout;
    @BindView(R.id.check_in_edit)
    ImageButton checkInEdit;
    @BindView(R.id.check_out_edit)
    ImageButton checkOutEdit;
    @BindView(R.id.check_in_edittext)
    EditText checkInEditText;
    @BindView(R.id.check_out_edittext)
    EditText checkOutEditText;
    private LatLng latLng;
    private LocationVO location;
    private String checkInTimeData;
    private String checkOutTimeData;
    private Dialog dialog;
    private ArrayAdapter<CharSequence> placeAdapter;
    private int arrayPlace;
    private String accommodationID;
    private DocumentReference mAccommodationRef;
    private boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            setTheme(R.style.AppThemeDark);
        }
        setContentView(R.layout.activity_upload);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(6);
            getSupportActionBar().setTitle(R.string.upload);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ButterKnife.bind(this);
        accommodationID = getIntent().getStringExtra(AccommodationInfoActivity.KEY_ACCOMMODATION_ID);
        mFirestore = FirebaseFirestore.getInstance();
        if (accommodationID != null) {
            isUpdate = true;
            mAccommodationRef = mFirestore.collection(UploadActivity.UPLOADREF).document(accommodationID);
            checkInEdit.setVisibility(View.VISIBLE);
            checkOutEdit.setVisibility(View.VISIBLE);
        }

        listAmenities = new ArrayList<>();
        mStroage = FirebaseStorage.getInstance().getReference();
        fileNameList = new ArrayList<>();
        fileDoneList = new ArrayList<>();
        uploadedImages = new ArrayList<>();
        FirebaseFirestore.setLoggingEnabled(true);
        uploadImageAdapter = new UploadImageAdapter(this, fileNameList, fileDoneList, uploadedImages);
        uploadImagesRV.setLayoutManager(new LinearLayoutManager(this));
        uploadImagesRV.setHasFixedSize(true);
        uploadImagesRV.setAdapter(uploadImageAdapter);

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                arrayPlace = getPlaceArray(i);
                placeAdapter = ArrayAdapter.createFromResource(UploadActivity.this, arrayPlace, android.R.layout.simple_spinner_item);
                placeSpinner.setAdapter(placeAdapter);
                placeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                if (loadedAccommodation != null) {
                    placeSpinner.setSelection(loadedAccommodation.getPlacePosition());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fileDoneList.size() == 0 || fileDoneList.get(fileDoneList.size() - 1).equals(UPLOADDONE)) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_LOAD_IMAGE1);
                } else {
                    Toast.makeText(UploadActivity.this, "Wait a little time to complete last upload", Toast.LENGTH_SHORT).show();
                }

            }
        });

        uploadMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UploadActivity.this, UploadMapActivity.class);
                if (latLng != null) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("LatLang", latLng);
                    intent.putExtra("LocationVO", bundle);
                }
                startActivityForResult(intent, RESULT_LOAD_MAP);
            }
        });

        checkInTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeUpload(UPLOADCHECKIN);
            }
        });

        checkOutTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeUpload(UPLOADCHECKOUT);
            }
        });

        checkInTimeData = getResources().getString(R.string.dummy_time);
        checkOutTimeData = getResources().getString(R.string.dummy_time);

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.loading_dialog);
        dialog.setCancelable(false);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        partTimeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    partTimePriceLayout.setVisibility(View.VISIBLE);
                } else {
                    partTimePriceLayout.setVisibility(View.GONE);
                }
            }
        });

        checkInEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInEditText.setVisibility(View.VISIBLE);
                checkInTime.setVisibility(View.GONE);
                isEdit = true;
            }
        });

        checkOutEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkOutEditText.setVisibility(View.VISIBLE);
                checkOutTime.setVisibility(View.GONE);
                isEdit = true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAccommodationRef != null) {
            mAccommodationRegistration = mAccommodationRef.addSnapshotListener(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAccommodationRegistration != null) {
            mAccommodationRegistration.remove();
            mAccommodationRegistration = null;
        }
    }

    private int getPlaceArray(int position) {

        switch (position) {
            case 0:
                return R.array.anywhere_array;

            case 1:
                return R.array.yangon_array;

            case 2:
                return R.array.mandalay_array;

            case 3:
                return R.array.bagan_array;

            case 4:
                return R.array.kyaik_hti_yoe_array;

            case 5:
                return R.array.hpa_an_array;

            case 6:
                return R.array.bago_array;


            case 7:
                return R.array.kalaw_array;

            case 8:
                return R.array.hsipaw_array;

            case 9:
                return R.array.taunggyi_array;


            case 10:
                return R.array.inle_array;


            case 11:
                return R.array.pyinoolwin_array;


            case 12:
                return R.array.pyay_array;


            case 13:
                return R.array.mt_naw_bu_baw_array;


            case 14:
                return R.array.gaw_yan_gyi_array;


            case 15:
                return R.array.chaung_tha_array;

            case 16:
                return R.array.ngwe_saung_array;


            case 17:
                return R.array.ngapali_array;


            case 18:
                return R.array.kaw_thoung_array;

            case 19:
                return R.array.myaik_array;

            default:
                return R.array.anywhere_array;

        }

    }

    private void timeUpload(int extra) {
        Intent intent = new Intent(UploadActivity.this, UploadTimeActivity.class);
        intent.putExtra(UPLOADTIME, extra);
        startActivityForResult(intent, extra);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE1 && resultCode == RESULT_OK) {
            if (data.getClipData() != null) {
                for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                    final Uri fileUri = data.getClipData().getItemAt(i).getUri();
                    final String fileName = getFileName(fileUri);
                    fileDoneList.add(UPLOADING);
                    fileNameList.add(fileName);
                    uploadImageAdapter.notifyDataSetChanged();
                    uploadImages(fileUri);
                }
            } else if (data.getData() != null) {
                final Uri fileUri = data.getData();
                fileNameList.add(getFileName(fileUri));
                fileDoneList.add(UPLOADING);
                uploadImageAdapter.notifyDataSetChanged();
                uploadImages(fileUri);

            }
        } else if (requestCode == RESULT_LOAD_MAP && resultCode == RESULT_OK) {
            Bundle bundle = data.getParcelableExtra("LocationVO");
            latLng = bundle.getParcelable("LocationVO");
            if (latLng != null) {
                location = new LocationVO(latLng.latitude, latLng.longitude);
            } else {
                location = new LocationVO(16.7739773, 96.1588279);
            }
            uploadMapButton.setText(String.valueOf(location.lat).concat(" , ").concat(String.valueOf(location.lang)));

        } else if (requestCode == UPLOADCHECKIN && resultCode == RESULT_OK) {
            checkInTimeData = data.getStringExtra(UPLOADTIME);
            checkInTime.setText(checkInTimeData);
            checkInTime.setTextSize(18);
            checkInTime.setTextColor(getResources().getColor(R.color.colorAccent));
        } else if (requestCode == UPLOADCHECKOUT && resultCode == RESULT_OK) {
            checkOutTimeData = data.getStringExtra(UPLOADTIME);
            checkOutTime.setText(checkOutTimeData);
            checkOutTime.setTextSize(18);
            checkOutTime.setTextColor(getResources().getColor(R.color.colorAccent));
        }
    }

    private void uploadImages(Uri fileUri) {
        final StorageReference fileToUpload = mStroage.child(UPLOADREF).child(getFileName(fileUri));
        fileToUpload.putFile(fileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                fileDoneList.remove(fileDoneList.size() - 1);
                fileDoneList.add(UPLOADDONE);
                fileToUpload.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        if (isUpdate) {
                            uploadedImages.addAll(loadedAccommodation.getPhotos());
                        }
                        uploadedImages.add(uri.toString());
                        uploadImageAdapter.notifyItemChanged(fileDoneList.size() - 1);
                        Toast.makeText(UploadActivity.this, "Uploaded Image", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        fileDoneList.remove(fileDoneList.size() - 1);
                        fileDoneList.add(UPLOADFAILED);
                        uploadImageAdapter.notifyItemChanged(fileDoneList.size() - 1);
                        Toast.makeText(UploadActivity.this, "Upload Failed", Toast.LENGTH_SHORT).show();
                    }
                });
                uploadImageAdapter.notifyDataSetChanged();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                fileDoneList.remove(fileDoneList.size() - 1);
                fileNameList.remove(fileNameList.size() - 1);
                uploadImageAdapter.notifyDataSetChanged();
                Toast.makeText(UploadActivity.this, "Uplaod Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.upload_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        showDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.confirm_menu) {
            if (uploadedImages.size() == 0) {
                uploadedImages.addAll(loadedAccommodation.getPhotos());
            }
            if (!nameField.getText().toString().equals("") && !phoneOneField.getText().toString().equals("") && uploadedImages.size() != 0 && !minPriceField.getText().toString().equals("") && !maxPriceField.getText().toString().equals("")) {
                dialog.show();
                uploadHotel();
            } else {
                final Snackbar snackbar = Snackbar.make(findViewById(R.id.upload_layout), "Please fill all requied fields.", Snackbar.LENGTH_INDEFINITE).setAction("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(UploadActivity.this, "Ok", Toast.LENGTH_SHORT).show();
                    }
                });
                snackbar.show();
            }
        } else if (item.getItemId() == android.R.id.home) {
            showDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(this);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.exit_layout);
        Button exit = dialog.findViewById(R.id.exit);
        Button cancel = dialog.findViewById(R.id.cancel);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UploadActivity.super.onBackPressed();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view != null) {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void uploadHotel() {
        CollectionReference collectionReference = mFirestore.collection(UPLOADREF);
        final String name = nameField.getText().toString();
        String phone1 = phoneOneField.getText().toString();
        String phone2 = phoneTwoField.getText().toString();
        String phone3 = phoneThreeField.getText().toString();
        String website = websiteField.getText().toString();
        String city = (String) citySpinner.getSelectedItem();
        String place = (String) placeSpinner.getSelectedItem();
        String accommodation = getResources().getStringArray(R.array.accommodation_array)[accommodationSpinner.getSelectedItemPosition()];
        String minPrice = minPriceField.getText().toString();
        String maxPrice = maxPriceField.getText().toString();
        String currecyType = getResources().getStringArray(R.array.currency_array)[currencySpinner.getSelectedItemPosition()];
        String address = uploadAddressField.getText().toString();
        boolean isPartTime = partTimeSwitch.isChecked();

        List<String> phoneNumbers = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            int position = i + 1;
            switch (position) {
                case 1:
                    if (!phone1.equals("")) {
                        phoneNumbers.add(phone1);
                    }
                    break;

                case 2:
                    if (!phone2.equals("")) {
                        phoneNumbers.add(phone2);
                    }
                    break;

                case 3:
                    if (!phone3.equals("")) {
                        phoneNumbers.add(phone3);
                    }
                    break;
            }

        }

        AccommodationVO accommodationData = new AccommodationVO();
        accommodationData.setName(name);
        accommodationData.setPhoneNumbers(phoneNumbers);
        accommodationData.setCity(city);
        accommodationData.setPlace(place);
        if (isUpdate && isEdit){
            checkInTimeData = checkInEditText.getText().toString();
            checkOutTimeData = checkOutEditText.getText().toString();
        }
        accommodationData.setCheckInTime(checkInTimeData);
        accommodationData.setCheckOutTime(checkOutTimeData);
        accommodationData.setAccommodationCategory(accommodation);
        accommodationData.setWebsiteUrl(website);
        accommodationData.setPartTime(isPartTime);
        if (isPartTime && !uploadPartTimePriceField.getText().toString().equals("")) {
            accommodationData.setPartTimePrice(Integer.valueOf(uploadPartTimePriceField.getText().toString()));
        }
        if (!minPrice.equals("")) {
            accommodationData.setMinPrice(Integer.valueOf(minPrice));
        }
        if (!maxPrice.equals("")) {
            accommodationData.setMaxPrice(Integer.valueOf(maxPrice));
        }
        if (currecyType.equals(getResources().getString(R.string.dollar))) {
            accommodationData.setCalculatedPrice(Integer.valueOf(minPrice) * AccommodationVO.CURRENCY_EXCHANGE);
        } else {
            accommodationData.setCalculatedPrice(Integer.valueOf(minPrice));
        }
        accommodationData.setAmenities(getAmenities());
        accommodationData.setCurrencyType(currecyType);
        accommodationData.setPlacePosition(placeSpinner.getSelectedItemPosition());
        accommodationData.setPhotos(uploadedImages);
        if (accommodationID != null) {
            location = loadedAccommodation.getLocation();
            accommodationData.setLocation(location);
        } else {
            accommodationData.setLocation(location);
        }
        accommodationData.setAddress(address);

        if (isUpdate) {
            accommodationData.setAvgRating(loadedAccommodation.getAvgRating());
            DocumentReference documentReference = collectionReference.document(accommodationID);
            documentReference.set(accommodationData).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    dialog.dismiss();
                    UploadActivity.super.onBackPressed();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(UploadActivity.this, "AccommodationVO Update Failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            collectionReference.add(accommodationData).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {

                }
            }).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {
                    dialog.dismiss();
                    UploadActivity.super.onBackPressed();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(UploadActivity.this, "AccommodationVO Upload Failed", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private List<Integer> getAmenities() {

        for (int i = 1; i <= 15; i++) {
            switch (i) {
                case AccommodationVO.ACCOMMODATION_AMENITIES_WIFI:
                    if (wifiSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_WIFI);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_POOL:
                    if (poolSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_POOL);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_SPA:
                    if (spaSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_SPA);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_RESTAURANTS:
                    if (restaurantSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_RESTAURANTS);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_BAR:
                    if (barSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_BAR);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_LAUNDRY:
                    if (laundrySwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_LAUNDRY);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_TV:
                    if (tvSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_TV);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_CAR_PARK:
                    if (carParkingSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_CAR_PARK);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_SHUTTLE:
                    if (shuttleSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_SHUTTLE);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_AIR_CON:
                    if (airConSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_AIR_CON);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_TELEPHONE:
                    if (telephoneSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_TELEPHONE);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_SHOWER:
                    if (showerSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_SHOWER);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_TOILET:
                    if (toiletSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_TOILET);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_REFRIGERATOR:
                    if (refrigeratorSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_REFRIGERATOR);
                    }
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_DISCOUNT:
                    if (discountSwitch.isChecked()) {
                        listAmenities.add(AccommodationVO.ACCOMMODATION_AMENITIES_DISCOUNT);
                    }
                    break;

                default:
                    break;
            }
        }

        return listAmenities;
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            try (Cursor cursor = getContentResolver().query(uri, null, null, null, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    @Override
    public void onEvent(DocumentSnapshot snapshot, FirebaseFirestoreException e) {
        onAccommodationLoaded(Objects.requireNonNull(snapshot.toObject(AccommodationVO.class)));
    }

    private void onAccommodationLoaded(AccommodationVO accommodation) {
        loadedAccommodation = accommodation;
        nameField.setText(accommodation.getName());
        for (int i = 0; i < accommodation.getPhoneNumbers().size(); i++) {
            switch (i) {
                case 0:
                    phoneOneField.setText(accommodation.getPhoneNumbers().get(0));
                    break;

                case 1:
                    phoneTwoField.setText(accommodation.getPhoneNumbers().get(1));
                    break;

                case 2:
                    phoneThreeField.setText(accommodation.getPhoneNumbers().get(2));
                    break;
            }
        }
        int position = 0;
        switch (accommodation.getCity()) {
            case "Anywhere":
                citySpinner.setSelection(0);
                position = 0;
                break;

            case "Yangon":
                citySpinner.setSelection(1);
                position = 1;
                break;

            case "Mandalay":
                citySpinner.setSelection(2);
                position = 2;
                break;

            case "Bagan":
                citySpinner.setSelection(3);
                position = 3;
                break;

            case "Kyaik Hti Yoe":
                citySpinner.setSelection(4);
                position = 4;
                break;

            case "Hpa-An":
                citySpinner.setSelection(5);
                position = 5;
                break;

            case "Bago":
                citySpinner.setSelection(6);
                position = 6;
                break;

            case "Kalaw":
                citySpinner.setSelection(7);
                position = 7;
                break;

            case "Taunggyi":
                citySpinner.setSelection(8);
                position = 8;
                break;

            case "Inle Lake":
                citySpinner.setSelection(9);
                position = 9;
                break;

            case "Pyin Oo Lwin":
                citySpinner.setSelection(10);
                position = 10;
                break;

            case "Pyay":
                citySpinner.setSelection(11);
                position = 11;
                break;

            case "Mount Naw Bu Baw":
                citySpinner.setSelection(12);
                position = 12;
                break;

            case "Taung Wine":
                citySpinner.setSelection(13);
                position = 13;
                break;

            case "Gaw Yan Gyi Island":
                citySpinner.setSelection(14);
                position = 14;
                break;

            case "Chaung Tha Beach":
                citySpinner.setSelection(15);
                position = 15;
                break;

            case "Ngwe Saung Beach":
                citySpinner.setSelection(16);
                position = 16;
                break;

            case "Ngapali Beach":
                citySpinner.setSelection(17);
                position = 17;
                break;

            case "Kaw Thoung":
                citySpinner.setSelection(17);
                position = 17;
                break;
        }

        placeAdapter = ArrayAdapter.createFromResource(UploadActivity.this, getPlaceArray(position), android.R.layout.simple_spinner_item);
//        placeSpinner.setAdapter(placeAdapter);
//        placeAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
//        int placePosition = accommodation.getPlacePosition();
//        placeSpinner.setSelection(placePosition);

        checkInEditText.setText(accommodation.getCheckInTime());
        checkOutEditText.setText(accommodation.getCheckOutTime());

        if (accommodation.getWebsiteUrl() != null) {
            websiteField.setText(accommodation.getWebsiteUrl());
        }

        if (accommodation.getAddress() != null) {
            uploadAddressField.setText(accommodation.getAddress());
        }

        switch (accommodation.getAccommodationCategory()) {
            case "Hotel":
                accommodationSpinner.setSelection(0);
                break;

            case "Resort":
                accommodationSpinner.setSelection(1);
                break;

            case "Motel":
                accommodationSpinner.setSelection(2);
                break;

            case "Hostel":
                accommodationSpinner.setSelection(3);
                break;

            case "Guest House":
                accommodationSpinner.setSelection(4);
                break;

            case "Inn":
                accommodationSpinner.setSelection(5);
                break;
        }

        minPriceField.setText(String.valueOf(accommodation.getMinPrice()));
        maxPriceField.setText(String.valueOf(accommodation.getMaxPrice()));
        checkInTime.setText(accommodation.getCheckInTime());
        checkInTime.setTextColor(getResources().getColor(R.color.colorAccent));
        checkOutTime.setText(accommodation.getCheckOutTime());
        checkOutTime.setTextColor(getResources().getColor(R.color.colorAccent));

        if (accommodation.getCurrencyType().equals(getResources().getString(R.string.dollar))) {
            currencySpinner.setSelection(0);
        } else {
            currencySpinner.setSelection(1);
        }

        for (int i = 0; i < accommodation.getAmenities().size(); i++) {
            int value = accommodation.getAmenities().get(i);
            switch (value) {
                case AccommodationVO.ACCOMMODATION_AMENITIES_WIFI:
                    wifiSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_POOL:
                    poolSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_SPA:
                    spaSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_RESTAURANTS:
                    restaurantSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_BAR:
                    barSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_LAUNDRY:
                    laundrySwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_TV:
                    tvSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_CAR_PARK:
                    carParkingSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_SHUTTLE:
                    shuttleSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_AIR_CON:
                    airConSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_TELEPHONE:
                    telephoneSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_SHOWER:
                    showerSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_TOILET:
                    toiletSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_REFRIGERATOR:
                    refrigeratorSwitch.setChecked(true);
                    break;

                case AccommodationVO.ACCOMMODATION_AMENITIES_DISCOUNT:
                    discountSwitch.setChecked(true);
                    break;

                default:
                    break;
            }
        }

        if (accommodation.getLocation() != null) {
            uploadMapButton.setText(accommodation.getLocation().toString());
        }

        if (accommodation.isPartTime()) {
            partTimeSwitch.setChecked(true);
            partTimePriceLayout.setVisibility(View.VISIBLE);
            uploadPartTimePriceField.setText(String.valueOf(accommodation.getPartTimePrice()));
        }
    }
}
