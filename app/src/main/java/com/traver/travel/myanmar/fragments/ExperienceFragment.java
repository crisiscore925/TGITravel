package com.traver.travel.myanmar.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.traver.travel.myanmar.R;


public class ExperienceFragment extends Fragment {

    public ExperienceFragment() {

    }


    public static ExperienceFragment newInstance() {
        return new ExperienceFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_experience, container, false);
    }
}
