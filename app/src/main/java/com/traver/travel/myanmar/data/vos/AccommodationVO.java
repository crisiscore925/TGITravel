package com.traver.travel.myanmar.data.vos;


import java.io.Serializable;
import java.util.List;

public class AccommodationVO implements Serializable {

    public static final String ACCOMMODATION_CITY = "city";
    public static final String ACCOMMODATION_PLACE = "place";
    public static final String ACCOMMODATION_CATEGORY = "accommodationCategory";
    public static final String ACCOMMODATION_MIN_PRICE = "minPrice";
    public static final String ACCOMMODATION_MAX_PRICE = "maxPrice";
    public static final String ACCOMMODATION_CALCULATED_PRICE = "calculatedPrice";
    public static final String ACCOMMODATION_AVG_RATING = "avgRating";
    public static final String ACCOMMODATION_PART_TIME= "partTime";
    public static final int CURRENCY_EXCHANGE = 1350;
    public static final int ACCOMMODATION_AMENITIES_WIFI = 1;
    public static final int ACCOMMODATION_AMENITIES_POOL = 2;
    public static final int ACCOMMODATION_AMENITIES_SPA = 3;
    public static final int ACCOMMODATION_AMENITIES_RESTAURANTS = 4;
    public static final int ACCOMMODATION_AMENITIES_BAR = 5;
    public static final int ACCOMMODATION_AMENITIES_LAUNDRY = 6;
    public static final int ACCOMMODATION_AMENITIES_TV = 7;
    public static final int ACCOMMODATION_AMENITIES_CAR_PARK = 8;
    public static final int ACCOMMODATION_AMENITIES_SHUTTLE = 9;
    public static final int ACCOMMODATION_AMENITIES_AIR_CON = 10;
    public static final int ACCOMMODATION_AMENITIES_TELEPHONE = 11;
    public static final int ACCOMMODATION_AMENITIES_SHOWER = 12;
    public static final int ACCOMMODATION_AMENITIES_TOILET = 13;
    public static final int ACCOMMODATION_AMENITIES_REFRIGERATOR = 14;
    public static final int ACCOMMODATION_AMENITIES_DISCOUNT = 15;

    private String name;
    private String city;
    private String accommodationCategory;
    private String place;
    private String address;
    private List<String> phoneNumbers;
    private int minPrice;
    private int maxPrice;
    private int numRatings;
    private double avgRating;
    private List<String> photos;
    private String currencyType;
    private List<Integer> amenities;
    private LocationVO location;
    private String checkInTime;
    private String checkOutTime;
    private String websiteUrl;
    private boolean isPartTime;
    private double calculatedPrice;
    private int partTimePrice;
    private int placePosition;


    public AccommodationVO(){}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public List<Integer> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Integer> amenities) {
        this.amenities = amenities;
    }

    public String getAccommodationCategory() {
        return accommodationCategory;
    }

    public void setAccommodationCategory(String accommodationCategory) {
        this.accommodationCategory = accommodationCategory;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }


    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getNumRatings() {
        return numRatings;
    }

    public void setNumRatings(int numRatings) {
        this.numRatings = numRatings;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public LocationVO getLocation() {
        return location;
    }

    public void setLocation(LocationVO location) {
        this.location = location;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public boolean isPartTime() {
        return isPartTime;
    }

    public void setPartTime(boolean partTime) {
        isPartTime = partTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getCalculatedPrice() {
        return calculatedPrice;
    }

    public void setCalculatedPrice(double calculatedPrice) {
        this.calculatedPrice = calculatedPrice;
    }

    public int getPartTimePrice() {
        return partTimePrice;
    }

    public void setPartTimePrice(int partTimePrice) {
        this.partTimePrice = partTimePrice;
    }

    public int getPlacePosition() {
        return placePosition;
    }

    public void setPlacePosition(int placePosition) {
        this.placePosition = placePosition;
    }
}
