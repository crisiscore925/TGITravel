package com.traver.travel.myanmar.adapters;

import android.content.Context;
import android.graphics.Point;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.request.RequestOptions;
import com.traver.travel.myanmar.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;
import java.util.Objects;


public class ImageViewPagerAdapter extends PagerAdapter implements View.OnClickListener {
    private Context context;
    private List<String> images;
    private Point p = new Point();
    private ViewPagerAdapterOnClickHandler mClickListener;
    private boolean isZoom;
    private AVLoadingIndicatorView avi;

    public interface ViewPagerAdapterOnClickHandler {
        void onClick(List<String> images, boolean isZoom);
    }

    public ImageViewPagerAdapter(Context context, ViewPagerAdapterOnClickHandler mClickListener, List<String> images, boolean isZoom) {
        this.images = images;
        this.context = context;
        this.mClickListener = mClickListener;
        this.isZoom = isZoom;
        Display display = ((WindowManager) Objects.requireNonNull(context.getSystemService(Context.WINDOW_SERVICE))).getDefaultDisplay();
        display.getSize(p);
    }

    @Override
    public int getCount() {
        if (images.size() == 0) {
            return 1;
        } else {
            return images.size();
        }
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        // Remove viewpager_item.xml from ViewPager
         container.removeView((View) object);

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @Override
    public Parcelable saveState() {
        return null;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup collection, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        view = Objects.requireNonNull(inflater).inflate(R.layout.detail_image_slider, collection, false);
        final ImageView img = view.findViewById(R.id.image);
        avi = view.findViewById(R.id.avi_adapter);
        startAnim();
        if (images.size() != 0) {
            String imgixUrl = images.get(position);
            if (!isZoom) {
                int imageViewWidth = p.x;
                ViewGroup.LayoutParams lp = img.getLayoutParams();
                lp.height = ((imageViewWidth / 4) * 3);
                img.setLayoutParams(lp);
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(context)
                        .load(imgixUrl)
                        .apply(new RequestOptions().centerCrop())
                        .into(img);

            } else {
                ViewGroup.LayoutParams lp = img.getLayoutParams();
                lp.height = p.y;
                img.setLayoutParams(lp);
                Glide.with(context)
                        .load(imgixUrl)
                        .apply(RequestOptions.fitCenterTransform())
                        .into(img);

            }
        } else {
            if (!isZoom) {
                int imageViewWidth = p.x;
                ViewGroup.LayoutParams lp = img.getLayoutParams();
                lp.height = ((imageViewWidth / 4) * 3);
                img.setLayoutParams(lp);
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(context)
                        .load(R.drawable.ic_hotel_dummy_background)
                        .apply(RequestOptions.centerCropTransform())
                        .into(img);

            } else {
                Glide.with(context)
                        .load(R.drawable.ic_hotel_dummy_background)
                        .into(img);

            }
        }

        img.setOnClickListener(this);
        collection.addView(view);

        return view;

    }

    private void startAnim() {
        avi.show();
        // or avi.smoothToShow();
    }


    @Override
    public void onClick(View view) {
        if (mClickListener == null) {
            return;
        }

        mClickListener.onClick(images, isZoom);
    }
}
