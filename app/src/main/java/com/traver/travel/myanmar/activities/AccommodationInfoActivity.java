package com.traver.travel.myanmar.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.Transaction;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.adapters.AccommodationListAdapter;
import com.traver.travel.myanmar.adapters.ImageViewPagerAdapter;
import com.traver.travel.myanmar.adapters.RatingAdapter;
import com.traver.travel.myanmar.adapters.SimilarAccommodationAdpater;
import com.traver.travel.myanmar.data.models.TRaverViewModel;
import com.traver.travel.myanmar.data.vos.AccommodationVO;
import com.traver.travel.myanmar.data.vos.RatingVO;
import com.traver.travel.myanmar.data.vos.UserVO;
import com.traver.travel.myanmar.utils.PrefUtils;
import com.traver.travel.myanmar.utils.RatingDialogFragment;
import com.traver.travel.myanmar.utils.ResourceUtil;
import com.traver.travel.myanmar.utils.ZoomOutPageTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class AccommodationInfoActivity extends AppCompatActivity implements EventListener<DocumentSnapshot>, RatingDialogFragment.RatingListener, ImageViewPagerAdapter.ViewPagerAdapterOnClickHandler, OnMapReadyCallback, AccommodationListAdapter.OnAccommodationSelectedListener, SimilarAccommodationAdpater.OnAccommodationSelectedListener {

    public static final String KEY_ACCOMMODATION_ID = "key_accommodation_id";
    public static final String KEY_ACCOMMODATION_REVIEW_NUMBER = "key_accommodation_review_number";
    public static final String KEY_ACCOMMODATION_REVIEW_AVG = "key_accommodation_review_avg";
    private static final String TAG = "AccommodationDetail";
    private static final int CALL_PERMISSION_REQUEST = 101;
    private static final float TOOLBAR_ELEVATION = 8;
    @BindView(R.id.image_view_pager)
    ViewPager imageViewPager;
    @BindView(R.id.accommodation_info_price)
    TextView accommodationInfoPrice;
    @BindView(R.id.accommodation_info_name)
    TextView accommodationInfoName;
    @BindView(R.id.accommodation_info_rating)
    MaterialRatingBar accommodationInfoRating;

    @BindView(R.id.accommodation_info_location)
    TextView accommodationInfoLocation;
    @BindView(R.id.amenities_icon1)
    ImageView ameenitiesIcon1;
    @BindView(R.id.amenities_icon2)
    ImageView ameenitiesIcon2;
    @BindView(R.id.amenities_icon3)
    ImageView ameenitiesIcon3;
    @BindView(R.id.amenities_icon4)
    ImageView ameenitiesIcon4;
    @BindView(R.id.amenities_icon5)
    ImageView ameenitiesIcon5;
    @BindView(R.id.amenities_label)
    TextView ameenitiesLabel;
    @BindView(R.id.amenities_mini)
    LinearLayout amenitiesMini;
    @BindView(R.id.show_more_btn)
    Button showLessBtn;
    @BindView(R.id.accommodation_info_min_price)
    TextView accommodationMinPrice;
    @BindView(R.id.accommodation_info_max_price)
    TextView accommodationMaxPrice;
    @BindView(R.id.accommodation_info_check_btn)
    Button accommodationInfoCheckBtn;
    @BindView(R.id.view_pager_fullscreen)
    ViewPager viewPagerFullScreen;
    @BindView(R.id.info_scroll_view)
    NestedScrollView scrollViewLayout;
    @BindView(R.id.review_total)
    TextView reveiewTotal;
    @BindView(R.id.add_review_btn)
    FloatingActionButton addReviewFloatingBtn;
    @BindView(R.id.no_similar)
    ImageView noSimilar;
    @BindView(R.id.no_similar_text)
    TextView noSimilarText;
    @BindView(R.id.accommodation_info_website)
    TextView websiteView;
    @BindView(R.id.accommodation_info_parttime)
    TextView partTimeView;
    @BindView(R.id.accommodation_info_parttime_price)
    TextView partTimePrice;
    public static final int RC_SIGN_IN = 9003;
    private DocumentReference mAccommodationRef;
    private ListenerRegistration mAccommodationRegistration;
    private boolean isZoomInfo = false;
    private GoogleMap mMap;
    private String accommodationLocation;
    private String accommodationName;
    private Marker marker;
    private RatingDialogFragment mRatingDialog;
    private RatingAdapter mRatingAdapter;
    private FirebaseFirestore mFirestore;
    @BindView(R.id.reviews_RV)
    RecyclerView mRatingsRecycler;
    @BindView(R.id.empty_reviews)
    ImageView mEmptyView;
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.accommodation_info_check_in)
    TextView checkInTime;
    @BindView(R.id.accommodation_info_check_out)
    TextView checkOutTime;
    @BindView(R.id.similar_accommodation_rv)
    RecyclerView similarAccommodationRV;
    @BindView(R.id.layoutDots)
    LinearLayout dotsLayout;
    @BindView(R.id.full_screen_qty)
    TextView fullScreenQty;
    @BindView(R.id.edit_info)
    FloatingActionButton editFAB;
    private AccommodationListAdapter mAdapter;
    private String accommodationID;
    private DocumentSnapshot currentSnapshot;
    private int imageQty;
    private SupportMapFragment accommodationMapView;
    private CollectionReference collectionReference;
    private ImageViewPagerAdapter adapter;
    private Intent intent;
    private String phonenumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            setTheme(R.style.AppThemeDark_NoActionBar);
        }
        setContentView(R.layout.activity_accommodation_info);
        ButterKnife.bind(this);

        accommodationID = getIntent().getStringExtra(KEY_ACCOMMODATION_ID);
        if (accommodationID == null) {
            throw new IllegalArgumentException("Must pass extra " + KEY_ACCOMMODATION_ID);
        }

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_white_24dp);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        }
        FirebaseFirestore.setLoggingEnabled(true);
        mFirestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        mFirestore.setFirestoreSettings(settings);
        mAccommodationRef = mFirestore.collection(UploadActivity.UPLOADREF).document(accommodationID);
        collectionReference = mFirestore.collection(UserVO.UPLOAD_USER);
        accommodationMapView = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.accommodation_info_map);
        accommodationMapView.getMapAsync(this);
        Query ratingsQuery = mAccommodationRef
                .collection("ratings")
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .limit(1);

        mRatingAdapter = new RatingAdapter(this, ratingsQuery) {
            @Override
            protected void onDataChanged() {
                if (getItemCount() == 0) {
                    mRatingsRecycler.setVisibility(View.GONE);
                    mEmptyView.setVisibility(View.VISIBLE);
                    reveiewTotal.setText(R.string.no_review);
                } else {
                    mRatingsRecycler.setVisibility(View.VISIBLE);
                    mEmptyView.setVisibility(View.GONE);
                    reveiewTotal.setText(getResources().getString(R.string.read_all).concat(" ").concat(getResources().getString(R.string.small_reviews)));
                }
            }
        };

        mRatingsRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRatingsRecycler.setAdapter(mRatingAdapter);
        mRatingDialog = new RatingDialogFragment();
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            if (firebaseUser.getUid().equals(getString(R.string.developer_uid)) || firebaseUser.getUid().equals(getString(R.string.developer_uid2))) {
                editFAB.setVisibility(View.VISIBLE);
                editFAB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(AccommodationInfoActivity.this, "Edit", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AccommodationInfoActivity.this, UploadActivity.class);
                        intent.putExtra(KEY_ACCOMMODATION_ID, accommodationID);
                        startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    public void onClick(List<String> images, boolean isZoom) {
        if (!isZoom) {
            isZoomInfo = true;
            if (Build.VERSION.SDK_INT >= 23) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
            scrollViewLayout.setVisibility(View.GONE);
            viewPagerFullScreen.setVisibility(View.VISIBLE);
            fullScreenQty.setVisibility(View.VISIBLE);
            fullScreenQty.setText("1".concat(" / ").concat(String.valueOf(imageQty)));
            adapter = new ImageViewPagerAdapter(this, this, images, isZoomInfo);
            viewPagerFullScreen.setAdapter(adapter);
            viewPagerFullScreen.addOnPageChangeListener(viewPagerPageChangeListener);
            viewPagerFullScreen.setPageTransformer(true, new ZoomOutPageTransformer());
            if (getSupportActionBar() != null) {
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
            }
        }
    }


    @Override
    public void onBackPressed() {
        if (isZoomInfo) {
            scrollViewLayout.setVisibility(View.VISIBLE);
            viewPagerFullScreen.setVisibility(View.GONE);
            fullScreenQty.setVisibility(View.GONE);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_white_24dp);
            }
            isZoomInfo = false;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (isZoomInfo) {
                scrollViewLayout.setVisibility(View.VISIBLE);
                viewPagerFullScreen.setVisibility(View.GONE);
                fullScreenQty.setVisibility(View.GONE);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_white_24dp);
                }
                isZoomInfo = false;
            } else {
                super.onBackPressed();
            }
        } else if (item.getItemId() == R.id.action_favorite) {
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                if (PrefUtils.getUserBookmarks(this).contains(accommodationID)) {
                    item.setIcon(R.drawable.ic_love_line);
                    PrefUtils.removeUserBookmarks(this, accommodationID);
                } else {
                    item.setIcon(R.drawable.ic_love_fill);
                    PrefUtils.addUserBookmarks(this, accommodationID);
                }
                updateUser(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
            } else {
                startSignIn();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateUser(FirebaseUser firebaseUser) {
        UserVO user = new UserVO();
        user.setUserId(firebaseUser.getUid());
        ArrayList<String> favList;
        favList = PrefUtils.getUserBookmarks(this);
        user.setUserFavList(favList);
        CollectionReference collectionReference = FirebaseFirestore.getInstance().collection(UserVO.UPLOAD_USER);
        collectionReference.document(firebaseUser.getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (aVoid != null) {
                    Toast.makeText(AccommodationInfoActivity.this, "Successfully added to your favourites", Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AccommodationInfoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.accommodation_info_menu, menu);
        if (PrefUtils.getUserBookmarks(this).contains(accommodationID)) {
            menu.getItem(0).setIcon(R.drawable.ic_love_fill);
        } else {
            menu.getItem(0).setIcon(R.drawable.ic_love_line);
        }
        return true;
    }

    @Override
    public void onEvent(DocumentSnapshot snapshot, FirebaseFirestoreException e) {
        if (e != null) {
            Log.w(TAG, "restaurant:onEvent", e);
            return;
        }
        currentSnapshot = snapshot;
        onAccommodationLoaded(snapshot.toObject(AccommodationVO.class));
    }

    private void onAccommodationLoaded(final AccommodationVO accommodation) {
        if (accommodation != null) {
            adapter = new ImageViewPagerAdapter(this, this, accommodation.getPhotos(), isZoomInfo);
            imageViewPager.setAdapter(adapter);
            imageViewPager.addOnPageChangeListener(viewPagerPageChangeListener);
            imageQty = accommodation.getPhotos().size();
            String price;
            String maxPrice;
            if (accommodation.getCurrencyType().equals("Dollar")) {
                price = getResources().getString(R.string.dollar_sign).concat(" ").concat(String.valueOf(accommodation.getMinPrice()));
                maxPrice = getResources().getString(R.string.dollar_sign).concat(" ").concat(String.valueOf(accommodation.getMaxPrice()));
            } else {
                price = String.valueOf(accommodation.getMinPrice()).concat(" ").concat(getResources().getString(R.string.kyats));
                maxPrice = String.valueOf(accommodation.getMaxPrice()).concat(" ").concat(getResources().getString(R.string.kyats));
            }
            addBottomDots(0);
            accommodationInfoPrice.setText(price);
            accommodationMinPrice.setText(price);
            accommodationMaxPrice.setText(maxPrice);

            if (accommodation.getWebsiteUrl() == null || accommodation.getWebsiteUrl().equals("")) {
                websiteView.setVisibility(View.GONE);
            } else {
                websiteView.setVisibility(View.VISIBLE);
                websiteView.setText(accommodation.getWebsiteUrl());
            }

            if (accommodation.isPartTime()) {
                partTimeView.setVisibility(View.VISIBLE);
                partTimePrice.setVisibility(View.VISIBLE);
                int num = accommodation.getPartTimePrice();
                int hour = num % 1000;
                int priceValue = (num / 1000) * 1000;
                String partimePrice;
                if (hour >= 10) {
                    partimePrice = String.valueOf(priceValue).concat(" Ks for ").concat("Day use");
                } else {
                    partimePrice = String.valueOf(priceValue).concat(" Ks for ").concat(String.valueOf(hour).concat(" Hour"));
                }
                partTimePrice.setText(partimePrice);
            } else {
                partTimeView.setVisibility(View.GONE);
                partTimePrice.setVisibility(View.GONE);
            }

            if (accommodation.getCheckInTime() != null) {
                checkInTime.setText(accommodation.getCheckInTime());
            }

            if (accommodation.getCheckOutTime() != null) {
                checkOutTime.setText(accommodation.getCheckOutTime());
            }

            if (accommodation.getName().equals("")) {
                accommodationName = getResources().getString(R.string.explore_accommodation_by_type);
            } else {
                accommodationName = accommodation.getName();
            }
            accommodationInfoName.setText(accommodationName);

            if (accommodation.getAddress() == null || accommodation.getAddress().equals("")) {
                if (accommodation.getPlace().equals(getResources().getString(R.string.anywhere))) {
                    accommodationLocation = accommodation.getCity();
                } else {
                    accommodationLocation = accommodation.getPlace().concat(" , ").concat(accommodation.getCity());
                }
                accommodationInfoLocation.setText(accommodationLocation);
            } else {
                accommodationLocation = accommodation.getPlace().concat(" , ").concat(accommodation.getCity());
                accommodationInfoLocation.setText(accommodation.getAddress().concat(" ,").concat(" ").concat(accommodationLocation));
            }

            accommodationInfoRating.setRating((float) accommodation.getAvgRating());

            ameenitiesIcon1.setImageResource(getImageID(accommodation.getAmenities().get(0)));
            ameenitiesIcon2.setImageResource(getImageID(accommodation.getAmenities().get(1)));
            ameenitiesIcon3.setImageResource(getImageID(accommodation.getAmenities().get(2)));
            ameenitiesIcon4.setImageResource(getImageID(accommodation.getAmenities().get(3)));
            ameenitiesIcon5.setImageResource(getImageID(accommodation.getAmenities().get(4)));
            if (accommodation.getAmenities().size() > 5) {
                int amenitiesLeft = accommodation.getAmenities().size() - 5;
                ameenitiesLabel.setText(getResources().getString(R.string.plus).concat(String.valueOf(amenitiesLeft)));
            } else {
                ameenitiesLabel.setVisibility(View.GONE);
            }
            final ArrayList<Integer> integers = new ArrayList<>(accommodation.getAmenities());
            amenitiesMini.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccommodationInfoActivity.this, AmenitiesListActivity.class);
                    intent.putIntegerArrayListExtra("AmenitiesList", integers);
                    startActivity(intent);
                }
            });


            showLessBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccommodationInfoActivity.this, AmenitiesListActivity.class);
                    intent.putIntegerArrayListExtra("AmenitiesList", integers);
                    startActivity(intent);
                }
            });

            accommodationInfoCheckBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String[] phonesNumbers = accommodation.getPhoneNumbers().toArray(new String[accommodation.getPhoneNumbers().size()]);
                    final boolean isDiscount = accommodation.getAmenities().contains(AccommodationVO.ACCOMMODATION_AMENITIES_DISCOUNT);
                    showDialogBox(phonesNumbers, isDiscount);
                }
            });

            if (accommodation.getLocation() != null) {
                LatLng latLng = new LatLng(accommodation.getLocation().lat, accommodation.getLocation().lang);
                setMarker(latLng);
                moveTo(CameraPosition.builder().target(latLng).zoom(17).build());
            }

            reveiewTotal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(AccommodationInfoActivity.this, ReviewsActivity.class);
                    intent.putExtra(KEY_ACCOMMODATION_ID, accommodationID);
                    intent.putExtra(KEY_ACCOMMODATION_REVIEW_NUMBER, accommodation.getNumRatings());
                    intent.putExtra(KEY_ACCOMMODATION_REVIEW_AVG, accommodation.getAvgRating());
                    startActivity(intent);
                }
            });

            addReviewFloatingBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (FirebaseAuth.getInstance().getCurrentUser() == null) {
                        startSignIn();
                    } else {
                        mRatingDialog.show(getSupportFragmentManager(), RatingDialogFragment.TAG);
                    }
                }
            });

            websiteView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String url = accommodation.getWebsiteUrl();
                    if (!url.startsWith("http://") && !url.startsWith("https://")) {
                        url = "http://" + url;
                    }
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                }
            });

            loadSimilarAccommodations(accommodation.getCity());

        }
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if (isZoomInfo) {
                fullScreenQty.setText(String.valueOf(++position).concat(" / ").concat(String.valueOf(imageQty)));
            } else {
                addBottomDots(position);
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void loadSimilarAccommodations(String city) {
        SimilarAccommodationAdpater similarAdapter;
        List<DocumentSnapshot> snapshots = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        similarAdapter = new SimilarAccommodationAdpater(getApplicationContext(), AccommodationInfoActivity.this, snapshots);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        similarAccommodationRV.setLayoutManager(linearLayoutManager);
        similarAccommodationRV.setAdapter(similarAdapter);
        snapshots.addAll(TRaverViewModel.getInstace().getSnapshots(city));
        similarAdapter.notifyDataSetChanged();
        if (TRaverViewModel.getInstace().getSnapshots(city).size() != 0) {
            noSimilarText.setVisibility(View.GONE);
            noSimilar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAccommodationSelected(DocumentSnapshot snapshot) {
        Intent intent = new Intent(AccommodationInfoActivity.this, AccommodationInfoActivity.class);
        intent.putExtra(AccommodationInfoActivity.KEY_ACCOMMODATION_ID, snapshot.getId());
        startActivity(intent);
    }

    @Override
    public void onAccommodationFavourited(String snapshotID) {
    }

    @Override
    public void onAccommodationRemoved(String snapshotID, int position) {

    }

    private void startSignIn() {
        Intent intent = new Intent(AccommodationInfoActivity.this, SignUpActivity.class);
        startActivityForResult(intent, AccommodationInfoActivity.RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AccommodationInfoActivity.RC_SIGN_IN) {
            if (resultCode == RESULT_OK && FirebaseAuth.getInstance().getCurrentUser() != null) {
                mRatingDialog.show(getSupportFragmentManager(), RatingDialogFragment.TAG);
                DocumentReference documentReference = collectionReference.document(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid());
                documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot snapshot) {
                        if (snapshot.exists()) {
                            PrefUtils.setUser(AccommodationInfoActivity.this, snapshot.toObject(UserVO.class));
                        } else {
                            uploadUser(Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()));
                        }
                    }
                });
            }
        }
    }

    private void uploadUser(FirebaseUser firebaseUser) {
        final UserVO user = new UserVO();
        user.setUserId(firebaseUser.getUid());
        ArrayList<String> favList = new ArrayList<>();
        user.setUserFavList(favList);
        collectionReference.document(firebaseUser.getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                PrefUtils.setUser(AccommodationInfoActivity.this, user);
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if (aVoid != null) {
                    PrefUtils.setUser(AccommodationInfoActivity.this, user);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(AccommodationInfoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                PrefUtils.setUser(AccommodationInfoActivity.this, user);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        mRatingAdapter.startListening();
        mAccommodationRegistration = mAccommodationRef.addSnapshotListener(this);

        scrollViewLayout.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == 0) {
                    toolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_white_24dp);
                    }
                } else if (oldScrollY > scrollY) {
                    toolbarAnimateShow(scrollY);
                } else {
                    toolbarAnimateHide();
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        mMap.clear();
        accommodationMapView.onStop();
        accommodationMapView.onDestroyView();
        mRatingAdapter.stopListening();

        if (mAccommodationRegistration != null) {
            mAccommodationRegistration.remove();
            mAccommodationRegistration = null;
        }
    }


    private void showDialogBox(String[] phonesNumbers, boolean isDiscount) {
        Dialog dialog = new Dialog(AccommodationInfoActivity.this);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.dialog_box_layout);
        Button phone1 = dialog.findViewById(R.id.phone1);
        Button phone2 = dialog.findViewById(R.id.phone2);
        Button phone3 = dialog.findViewById(R.id.phone3);
        TextView discount = dialog.findViewById(R.id.discount);

        if (isDiscount) {
            discount.setVisibility(View.VISIBLE);
        } else {
            discount.setVisibility(View.GONE);
        }

        for (int i = 0; i < phonesNumbers.length; i++) {
            if (!phonesNumbers[i].equals("")) {
                switch (i) {
                    case 0:
                        getActionCall(phone1, phonesNumbers[i]);
                        break;
                    case 1:
                        getActionCall(phone2, phonesNumbers[i]);
                        break;
                    case 2:
                        getActionCall(phone3, phonesNumbers[i]);
                        break;
                }
            }
        }

        dialog.show();
    }

    private void getActionCall(Button phoneView, final String phonesNumber) {
        phonenumber = phonesNumber;
        phoneView.setVisibility(View.VISIBLE);
        phoneView.setText("0".concat(phonesNumber));
        phoneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse(getResources().getString(R.string.tel).concat(phonesNumber)));
                if (ActivityCompat.checkSelfPermission(AccommodationInfoActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(AccommodationInfoActivity.this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSION_REQUEST);
                } else {
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CALL_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(intent);
                } else {
                    intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(getResources().getString(R.string.tel).concat(phonenumber)));
                    startActivity(intent);
                }
        }
    }

    private void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[imageQty];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0) {
            dots[currentPage].setTextColor(colorsActive[currentPage]);
        }
    }

    private int getImageID(Integer integer) {
        int resID;
        switch (integer) {

            case 1:
                resID = R.drawable.ic_wifi_black_24dp;
                break;

            case 2:
                resID = R.drawable.ic_pool_black_24dp;
                break;

            case 3:
                resID = R.drawable.ic_spa_black_24dp;
                break;

            case 4:
                resID = R.drawable.ic_restaurant_menu_black_24dp;
                break;

            case 5:
                resID = R.drawable.ic_local_bar_black_24dp;
                break;

            case 6:
                resID = R.drawable.ic_local_laundry_service_black_24dp;
                break;

            case 7:
                resID = R.drawable.ic_tv_black_24dp;
                break;

            case 8:
                resID = R.drawable.ic_local_parking_black_24dp;
                break;

            case 9:
                resID = R.drawable.ic_airport_shuttle_black_24dp;
                break;

            case 10:
                resID = R.drawable.ic_ac_unit_black_24dp;
                break;

            case 11:
                resID = R.drawable.ic_phone_in_talk_black_24dp;
                break;

            case 12:
                resID = R.drawable.ic_bathtub_with_opened_shower;
                break;

            case 13:
                resID = R.drawable.ic_toilet;
                break;

            case 14:
                resID = R.drawable.ic_refrigerator;
                break;

            case 15:
                resID = R.drawable.ic_access_time_black_24dp;
                break;
            default:
                resID = R.mipmap.ic_launcher;
        }
        return resID;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(16.7739773, 96.1588279);
        setMarker(latLng);
    }

    private void setMarker(final LatLng latLng) {
        if (marker != null) {
            marker.remove();
        }
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLngNew) {
                Bundle args = new Bundle();
                args.putParcelable("LatLang", latLng);
                Intent intent = new Intent(AccommodationInfoActivity.this, MapActivity.class);
                intent.putExtra("Name", accommodationName);
                intent.putExtra("Place", accommodationLocation);
                intent.putExtra("LocationVO", latLng);
                intent.putExtra("bundle", args);
                startActivity(intent);
            }
        });
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(latLng);
        circleOptions.radius(100);
        circleOptions.fillColor(Color.argb(60, 75, 183, 232));
        circleOptions.strokeColor(Color.argb(255, 96, 125, 139));
        circleOptions.strokeWidth(4);
        mMap.addCircle(circleOptions);
        marker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(ResourceUtil.getBitmap(this, R.drawable.ic_map_pin_24dp)))
                .position(latLng).draggable(false));
        moveTo(CameraPosition.builder().target(latLng).zoom(17).build());
    }

    private void moveTo(CameraPosition cp) {
        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp), 1000, null);
        }
    }

    private Task<Void> addRating(final DocumentReference documentReference, final RatingVO rating) {

        final DocumentReference ratingRef = documentReference.collection("ratings").document();

        return mFirestore.runTransaction(new Transaction.Function<Void>() {
            @Nullable
            @Override
            public Void apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                AccommodationVO accommodation = transaction.get(documentReference).toObject(AccommodationVO.class);
                int newNumberRatings;
                if (accommodation != null) {
                    newNumberRatings = accommodation.getNumRatings() + 1;
                    double oldRatingTotal = accommodation.getAvgRating() * accommodation.getNumRatings();
                    double newAvgRating = (oldRatingTotal + rating.getRating()) / newNumberRatings;
                    accommodation.setNumRatings(newNumberRatings);
                    accommodation.setAvgRating(newAvgRating);
                    transaction.set(documentReference, accommodation);
                    transaction.set(ratingRef, rating);
                }
                return null;
            }
        });
    }

    @Override
    public void onRating(RatingVO rating) {
        addRating(mAccommodationRef, rating).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "RatingVO added");

                // Hide keyboard and scroll to top
                hideKeyboard();
                mRatingsRecycler.smoothScrollToPosition(0);
            }
        })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Add rating failed", e);

                        // Show failure message and hide keyboard
                        hideKeyboard();
                        Snackbar.make(findViewById(android.R.id.content), "Failed to add rating",
                                Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)))
                    .hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void toolbarSetElevation(float elevation) {
        // setElevation() only works on Lollipop
        if (Build.VERSION.SDK_INT >= 21) {
            toolbar.setElevation(elevation);
        }
    }

    private void toolbarAnimateShow(final int verticalOffset) {
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
        }
        toolbar.animate()
                .translationY(0)
                .setInterpolator(new LinearInterpolator())
                .setDuration(180)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        toolbarSetElevation(verticalOffset == 0 ? 0 : TOOLBAR_ELEVATION);
                    }
                });
    }

    private void toolbarAnimateHide() {
        toolbar.animate()
                .translationY(-toolbar.getHeight())
                .setInterpolator(new LinearInterpolator())
                .setDuration(180)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        toolbarSetElevation(0);
                    }
                });
    }


}
