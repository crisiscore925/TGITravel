package com.traver.travel.myanmar.activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.utils.ResourceUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    @BindView(R.id.accommodation_name)
    TextView accommodationName;
    @BindView(R.id.location_city)
    TextView accommodationCity;
    private GoogleMap mMap;
    private LatLng latLng;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }else {
            setTheme(R.style.AppThemeDark);
        }
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));
        }

        String name = getIntent().getStringExtra("Name");
        String place = getIntent().getStringExtra("Place");
        Bundle bundle = getIntent().getParcelableExtra("bundle");
        latLng = bundle.getParcelable("LatLang");
         mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.full_screen_map);
        mapFragment.getMapAsync(this);
        accommodationCity.setText(place);
        accommodationName.setText(name);
        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle(name.concat(" ").concat(getResources().getString(R.string.accommodation_location)));
        }
        accommodationName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveTo(CameraPosition.builder().target(latLng).zoom(16).build());
            }
        });

        accommodationCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveTo(CameraPosition.builder().target(latLng).zoom(16).build());
            }
        });
    }

    private void moveTo(CameraPosition cp) {
        if (mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp), 1000, null);
        }
    }

    @Override
    protected void onStop() {
        if (mapFragment != null){
            mapFragment.onDestroyView();
        }
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(latLng);
        circleOptions.radius(300);
        circleOptions.fillColor(Color.argb(80, 75, 183, 232));
        circleOptions.strokeColor(Color.argb(255, 96, 125, 139));
        circleOptions.strokeWidth(4);

        mMap.addCircle(circleOptions);
        mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(ResourceUtil.getBitmap(this, R.drawable.ic_map_pin_24dp)))
                .position(latLng).draggable(false));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
    }
}
