package com.traver.travel.myanmar.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.activities.AccommodationListActivity;
import com.traver.travel.myanmar.adapters.HotelAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExploreFragment extends Fragment implements HotelAdapter.HotelAdapterOnClickHandler {

    @BindView(R.id.hotel_rv)
    RecyclerView hotelRV;
    @BindView(R.id.places_rv)
    RecyclerView placesRV;
    @BindView(R.id.explore_search_button)
    Button exploreSearchButton;
    public static final int VIEW_BY_TYPE = 1;
    public static final int VIEW_BY_PLACE = 2;
    private SearchButtonClickListener listener;

    public ExploreFragment() {

    }

    public interface SearchButtonClickListener {
        void onButtonClicked();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (SearchButtonClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnHeadlineSelectedListener");
        }
    }

    public static ExploreFragment newInstance() {
        return new ExploreFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        ButterKnife.bind(this, view);
        String[] names = getResources().getStringArray(R.array.accommodation_array);
        String[] places = getResources().getStringArray(R.array.upload_cities_array);
        int[] resources = getAccommodationResources();
        int[] placeResources = getPlacesResources();
        HotelAdapter adapter = new HotelAdapter(getContext(), ExploreFragment.this, names, resources, VIEW_BY_TYPE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        LinearLayoutManager placesLayoutManager = new LinearLayoutManager(getContext());
        placesLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        hotelRV.setLayoutManager(layoutManager);
        placesRV.setLayoutManager(placesLayoutManager);
        hotelRV.setItemAnimator(new DefaultItemAnimator());
        placesRV.setItemAnimator(new DefaultItemAnimator());
        hotelRV.setAdapter(adapter);
        HotelAdapter placesAdapter = new HotelAdapter(getContext(), ExploreFragment.this, places, placeResources, VIEW_BY_PLACE);
        placesRV.setAdapter(placesAdapter);
        exploreSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonClicked();
            }
        });
        return view;
    }

    private int[] getAccommodationResources() {
        return new int[]{R.drawable.hotel_image, R.drawable.resort, R.drawable.motel, R.drawable.hostel, R.drawable.guest_house, R.drawable.inn};
    }

    private int[] getPlacesResources() {
        return new int[]{R.drawable.yangon, R.drawable.mandalay, R.drawable.bagan, R.drawable.kyaik_hti_yoe, R.drawable.hpa_an, R.drawable.bago,
                R.drawable.kalaw , R.drawable.hsipaw, R.drawable.taunggyi, R.drawable.inle, R.drawable.pyin_oo_lwin, R.drawable.pyay, R.drawable.naw_bu_baw
                , R.drawable.gawyingyi, R.drawable.chaung_tha, R.drawable.ngwe_saung, R.drawable.ngapali, R.drawable.kaw_taung , R.drawable.myaik};
    }

    @Override
    public void onClick(int position, int viewType) {
        Intent intent = new Intent(getActivity(), AccommodationListActivity.class);

        if (viewType == VIEW_BY_TYPE) {
            switch (position) {
                case 0:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.HOTELQUERYID);
                    break;

                case 1:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.RESORTQUERYID);
                    break;

                case 2:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.MOTELQUERYID);

                    break;

                case 3:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.HOSTELQUERYID);
                    break;

                case 4:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.GUESTHOUSEQUERYID);
                    break;

                case 5:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.INNQUERYID);
                    break;

                default:
                    Toast.makeText(getContext(), getResources().getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                    intent.putExtra(AccommodationListActivity.QUERYID, -1);
                    break;
            }
        } else if (viewType == VIEW_BY_PLACE) {
            switch (position) {
                case 0:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.YANGONQUERYID);
                    break;

                case 1:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.MANDALAYQUERYID);
                    break;

                case 2:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.BAGANQUERYID);

                    break;

                case 3:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.KYAIKHTIYOEQUERYID);
                    break;

                case 4:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.HPA_ANQUERYID);
                    break;

                case 5:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.BAGOQUERYID);
                    break;

                case 6:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.KALAWQUERYID);
                    break;

                case 7:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.HSIPAWQUERYID);
                    break;

                case 8:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.TAUNGGYIQUERYID);
                    break;
                case 9:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.INLEQUERYID);
                    break;
                case 10:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.PYINOOLWINQUERYID);
                    break;
                case 11:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.PYAYQUERYID);
                    break;
                case 12:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.NAWBUBAWQUERYID);
                    break;
                case 13:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.GAWYANGYIQUERYID);
                    break;
                case 14:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.CHAUNGTHAQUERYID);
                    break;
                case 15:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.NGWESAUNGQUERYID);
                    break;
                case 16:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.NGAPALIQUERYID);
                    break;
                case 17:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.KAWTHOUNGQUERYID);
                    break;

                case 18:
                    intent.putExtra(AccommodationListActivity.QUERYID, AccommodationListActivity.MYAIKISLANDQUERYID);
                    break;

                default:
                    Toast.makeText(getContext(), getResources().getString(R.string.similar_accommodations), Toast.LENGTH_SHORT).show();
                    intent.putExtra(AccommodationListActivity.QUERYID, -1);
                    break;
            }
        }
        startActivity(intent);
    }
}
