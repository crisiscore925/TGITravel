package com.traver.travel.myanmar.data.vos;


import java.util.ArrayList;

public class UserVO {
    private String userId;
    private ArrayList<String> userFavList;
    public static final String UPLOAD_USER = "TRavers";

    public UserVO() {
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<String> getUserFavList() {
        return userFavList;
    }

    public void setUserFavList(ArrayList<String> userFavList) {
        this.userFavList = userFavList;
    }
}
