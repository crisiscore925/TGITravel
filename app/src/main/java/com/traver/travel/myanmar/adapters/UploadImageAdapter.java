package com.traver.travel.myanmar.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.activities.UploadActivity;
import java.util.List;

public class UploadImageAdapter extends RecyclerView.Adapter<UploadImageAdapter.ViewHolder> {
    private List<String> fileNameList;
    private List<String> fileDoneList;
    private List<String> uploadedImages ;
    private Context mContext;

    public UploadImageAdapter(Context context , List<String> fileNameList, List<String> fileDoneList, List<String> uploadedImages) {
        this.fileNameList = fileNameList;
        this.fileDoneList = fileDoneList;
        this.uploadedImages = uploadedImages;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.images_list_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String fileName = fileNameList.get(position);
        holder.fileNameView.setText(fileName);
        String uploadDone = fileDoneList.get(position);
        switch (uploadDone) {
            case UploadActivity.UPLOADING:
                holder.fileDoneView.setImageResource(R.drawable.ic_cloud_upload_black_24dp);
                break;
            case UploadActivity.UPLOADFAILED:
                holder.fileDoneView.setImageResource(R.drawable.ic_error_outline_black_24dp);
                break;
            default:
                holder.fileDoneView.setImageResource(R.drawable.ic_cloud_done_black_24dp);
                int size = uploadedImages.size();
                if (size != 0 && size == fileDoneList.size()) {
                    Glide.with(mContext)
                            .load(uploadedImages.get(fileDoneList.size() - 1))
                            .apply(RequestOptions.bitmapTransform(new CenterCrop()))
                            .into(holder.fileImageView);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return fileNameList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView fileNameView;
        ImageView fileDoneView;
        ImageView fileImageView;

        ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            fileImageView = itemView.findViewById(R.id.upload_image);
            fileNameView = itemView.findViewById(R.id.upload_file_name);
            fileDoneView = itemView.findViewById(R.id.upload_done_image);
        }

    }
}
