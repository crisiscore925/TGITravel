package com.traver.travel.myanmar.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.Query;
import com.traver.travel.myanmar.R;
import com.traver.travel.myanmar.data.vos.AccommodationVO;
import com.traver.travel.myanmar.utils.PrefUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class AccommodationListAdapter extends FireStoreAdapter<RecyclerView.ViewHolder> {

    // An Activity's Context.
    private Context mContext;

    // The list of menu items.
    private List<DocumentSnapshot> mItems;

    private OnAccommodationSelectedListener mListener;


    public AccommodationListAdapter(Context context, OnAccommodationSelectedListener listener, List<DocumentSnapshot> recyclerViewItems , Query query) {
        super(query);
        this.mContext = context;
        this.mListener = listener;
        this.mItems = recyclerViewItems;
    }

    public interface OnAccommodationSelectedListener {
        void onAccommodationSelected(DocumentSnapshot snapshot);

        void onAccommodationFavourited(String snapshotID);

        void onAccommodationRemoved(String snapshotID, int position);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.accommodation_card, parent, false);
        return new AccommodationListAdapter.AccommodationViewHolder(menuItemLayoutView);
    }


    @Override
    public void onViewDetachedFromWindow(@NonNull RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.destroyDrawingCache();
    }

    public class AccommodationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.accommodation_image)
        ImageView accommodationCardImage;
        @BindView(R.id.accommodation_card_name)
        TextView accommodationCardName;
        @BindView(R.id.accommodation_card_location)
        TextView accommodationCardLocation;
        @BindView(R.id.accommodation_card_rating)
        MaterialRatingBar accommodationCardRatingBar;
        @BindView(R.id.accommodation_card_price)
        TextView accommodationCardPrice;
        @BindView(R.id.favourite_button)
        ImageButton acccommodationFavButton;

        AccommodationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void bind(final DocumentSnapshot snapshot, final OnAccommodationSelectedListener listener) {
            AccommodationVO accommodation = new AccommodationVO();
            boolean isFavourite = false;
            if (snapshot != null) {
                accommodation = snapshot.toObject(AccommodationVO.class);
                isFavourite = PrefUtils.getUserBookmarks(mContext).contains(snapshot.getId());
            }

            if ((accommodation != null ? accommodation.getPhotos().size() : 0) != 0) {

                Glide.with(accommodationCardImage.getContext())
                        .load(accommodation.getPhotos().get(0))
                        .into(accommodationCardImage);
            } else {
                Glide.with(accommodationCardImage.getContext())
                        .load(R.drawable.ic_hotel_dummy_background)
                        .apply(RequestOptions.fitCenterTransform())
                        .into(accommodationCardImage);
            }

            if (accommodation != null) {
                if (accommodation.getName().equals("")) {
                    accommodationCardName.setText(R.string.explore_accommodation_by_type);
                } else {
                    accommodationCardName.setText(accommodation.getName());
                }
            }

            String location;
            if (accommodation != null) {
                if (accommodation.getPlace().equals(mContext.getResources().getString(R.string.anywhere))) {
                    location = accommodation.getCity();
                } else {
                    location = accommodation.getPlace().concat(" , ").concat(accommodation.getCity());
                }
                accommodationCardLocation.setText(location);
            }
            String price = " ";
            if (accommodation != null) {
                if (accommodation.getCurrencyType().equals("Dollar")) {
                    price = itemView.getResources().getString(R.string.dollar_sign).concat(" ").concat(String.valueOf(accommodation.getMinPrice()));
                } else {
                    price = String.valueOf(accommodation.getMinPrice()).concat(" ").concat(itemView.getResources().getString(R.string.kyats));
                }
            }

            accommodationCardPrice.setText(price);

            if (accommodation != null) {
                if (accommodation.getAvgRating() != 0) {
                    accommodationCardRatingBar.setRating((float) accommodation.getAvgRating());
                } else {
                    accommodationCardRatingBar.setRating(0);
                }
            }

            if (isFavourite) {
                acccommodationFavButton.setImageResource(R.drawable.ic_love_fill);
            } else {
                acccommodationFavButton.setImageResource(R.drawable.ic_love_line);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onAccommodationSelected(snapshot);
                    }
                }
            });

            acccommodationFavButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        if (snapshot != null) {
                            listener.onAccommodationFavourited(snapshot.getId());
                        }

                        if (snapshot != null) {
                            listener.onAccommodationRemoved(snapshot.getId(), getAdapterPosition());
                            if (PrefUtils.getUserBookmarks(mContext).contains(snapshot.getId())) {
                                acccommodationFavButton.setImageResource(R.drawable.ic_love_fill);
                            } else {
                                acccommodationFavButton.setImageResource(R.drawable.ic_love_line);
                            }
                        }
                    }
                }
            });
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AccommodationViewHolder menuItemHolder = (AccommodationViewHolder) holder;
        menuItemHolder.bind(mItems.get(position), mListener);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
