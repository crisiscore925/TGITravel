package com.traver.travel.myanmar;

import android.app.Application;

import com.traver.travel.myanmar.utils.ConnectivityReceiver;

public class TRaveApp extends Application {
    private static TRaveApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                e.printStackTrace();
            }
        });
        mInstance = this;

    }

    public static synchronized TRaveApp getInstance() {
        if (mInstance == null) mInstance = new TRaveApp();
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
